################################################################################
# Package: OTMonitor
################################################################################
gaudi_subdir(OTMonitor v3r10)

gaudi_depends_on_subdirs(Det/OTDet
                         Event/DigiEvent
                         Event/LinkerEvent
                         Event/MCEvent
                         GaudiAlg)

find_package(AIDA)

find_package(ROOT)
find_package(Boost)
include_directories(SYSTEM ${ROOT_INCLUDE_DIRS} ${Boost_INCLUDE_DIRS})

gaudi_add_module(OTMonitor
                 src/*.cpp
                 INCLUDE_DIRS AIDA Event/DigiEvent
                 LINK_LIBRARIES OTDetLib LinkerEvent MCEvent GaudiAlgLib)

