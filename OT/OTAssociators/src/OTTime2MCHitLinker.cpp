// Linker
#include "Linker/LinkerTool.h"

// MCEvent
#include "Event/MCHit.h"
#include "Event/MCOTDeposit.h"
#include "Event/OTTime.h"

// local
#include "OTTime2MCHitLinker.h"

/** @file OTTime2MCHitLinker.cpp 
 *
 *  Implementation of the OTTime2MCHitLinker class
 *  
 *  @author J. Nardulli and J. van Tilburg
 *  @date   15/06/2004
 */

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( OTTime2MCHitLinker )

OTTime2MCHitLinker::OTTime2MCHitLinker( const std::string& name,
					ISvcLocator* pSvcLocator )
  : GaudiAlgorithm ( name , pSvcLocator ) 
{
  declareProperty( "OutputData", m_outputData = LHCb::OTTimeLocation::Default + "2MCHits" );
  declareProperty( "SpillOver", m_spillOver = false );
}

StatusCode OTTime2MCHitLinker::execute() 
{
  // Get OTTimes
   LHCb::OTTimes* timeCont = get<LHCb::OTTimes>( LHCb::OTTimeLocation::Default );
  // Get MCHits; no spillover 
  LHCb::MCHits* mcHits = get<LHCb::MCHits>( LHCb::MCHitLocation::OT );

  // Create a linker
  LinkerWithKey<LHCb::MCHit,LHCb::OTTime> myLink( evtSvc(), msgSvc(), outputData() );
  
  // loop and link OTTimes to MC truth
  for (const auto& time : *timeCont) {
    // Find and link all hits 
    std::vector<const LHCb::MCHit*> hitVec;
    StatusCode sc = associateToTruth( time, hitVec, mcHits );
    if ( !sc.isSuccess() ) return Error( "Failed to associate to truth" , sc );
    for (const auto& hit : hitVec ) myLink.link( time , hit );
  }

  return StatusCode::SUCCESS;
}

StatusCode OTTime2MCHitLinker::associateToTruth( const LHCb::OTTime* aTime,
						 std::vector<const LHCb::MCHit*>& hitVec, 
						 LHCb::MCHits* mcHits )
{
  // Make link to MCHit from OTTime
  LinkerTool<LHCb::OTTime, LHCb::MCOTDeposit> associator( evtSvc(), LHCb::OTTimeLocation::Default + "2MCDeposits" );
  const auto* aTable = associator.direct();
  if( !aTable ) return Error( "Failed to find table", StatusCode::FAILURE );

  auto range = aTable->relations( aTime );
  for ( auto iterDep = range.begin() ; iterDep != range.end() ; ++iterDep ) {
    const LHCb::MCOTDeposit* aDeposit = iterDep->to();
    if ( aDeposit ) {
      const LHCb::MCHit* aHit = aDeposit->mcHit();
      if ( aHit && ( m_spillOver || ( mcHits == aHit->parent() ) ) ){
        hitVec.push_back( aHit );
      }
    }
  }
  
  return StatusCode::SUCCESS;
}
