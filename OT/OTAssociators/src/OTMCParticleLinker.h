#ifndef OTASSOCIATORS_OTMCPARTICLELINKER_H 
#define OTASSOCIATORS_OTMCPARTICLELINKER_H 1

// Include files
// from STD
#include <string>
#include <vector>

// from MCEvent
#include "Event/MCParticle.h"
#include "Event/MCOTTime.h"

// Base class
#include <Associators/Linker.h>

/** @class OTMCParticleLinker OTMCParticleLinker.h
 *
 *  Algorithm to link OTChannelIDs to OT MCParticles.
 *
 *  The MC linked particles for a given channelID(key) can be retrieved, for example, in the following way
 *
 *  @code
 *  #include "Event/MCParticle.h"
 *  #include "Kernel/OTChannelID.h"
 *  #include "Event/OTTime.h"
 *  #include <Assiciators/InputLinks.h>
 *
 *  ...
 *
 *  void OTMCParticleLinker::operator()(const LHCb::OTTimes& times,
 *                                      const LHCb::LinksByKey& links) const
 *  {
 *    InputLinks<ContainedObject, LHCb::MCParticle> inputLinks(links);
 *
 *    for (const auto time : times) {
 *      // Get channelID(key) from OTTime
 *      unsigned int key = time->channel());
 *
 *      for (auto entry : intputLinks.from(key)) {
 *        auto particle = entry.to();
 *        ...
 *      }
 *      ...
 *    }
 *  ...
 *  }
 *  @endcode
 *
 *  For the reverse one can do
 *
 *  @code
 *  void OTMCParticleLinker::operator()(const LHCb::MCParticles& particles,
 *                                      const LHCb::LinksByKey& links) const
 *  {
 *    InputLinks<ContainedObject, LHCb::MCParticle> inputLinks(links);
 *
 *    for (auto particle : particles) {
 *      for (auto entry : inputLinks.to(particle) {
 *        const auto otTime = entry.from();
 *        ...
 *      }
 *    }
 *  }
 *  @endcode
 *
 *  @author Roel Aaij
 *  @date   2017-02-17
 */

class OTMCParticleLinker
   : public Gaudi::Functional::Linker<LHCb::LinksByKey(const LHCb::MCOTTimes&,
                                                       const LHCb::LinksByKey&)>
{
public:

   //Standard constructor
   OTMCParticleLinker(const std::string& name, ISvcLocator* pSvcLocator);

   LHCb::LinksByKey operator()(const LHCb::MCOTTimes& clusters,
                               const LHCb::LinksByKey& links) const override;
};

#endif // OTASSOCIATORS_OTMCPARTICLELINKER_H
