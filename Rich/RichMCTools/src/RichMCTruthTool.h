
//-----------------------------------------------------------------------------
/** @file RichMCTruthTool.h
 *
 *  Header file for tool : Rich::MC::MCTruthTool
 *
 *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date   15/03/2002
 */
//-----------------------------------------------------------------------------

#pragma once

// STL
#include <algorithm>
#include <memory>
#include <string>

// from Gaudi
#include "GaudiKernel/IIncidentListener.h"
#include "GaudiKernel/IIncidentSvc.h"

// base class
#include "RichKernel/RichToolBase.h"

// Kernel
#include "Kernel/RichParticleIDType.h"

// Rich Utils
#include "RichUtils/RichHashMap.h"
#include "RichUtils/RichMap.h"
#include "RichUtils/RichPixelCluster.h"

// Event model
#include "Event/MCParticle.h"
#include "Event/MCRichDigit.h"
#include "Event/MCRichDigitSummary.h"
#include "Event/MCRichHit.h"
#include "Event/MCRichOpticalPhoton.h"
#include "Event/MCRichSegment.h"
#include "Event/MCRichTrack.h"
#include "Event/MCTruth.h"
#include "Event/Track.h"
#include "Kernel/RichParticleIDType.h"

// Linkers
#include "Linker/LinkedTo.h"
#include "Linker/LinkerTool.h"

// Interfaces
#include "MCInterfaces/IRichMCTruthTool.h"

namespace Rich::MC
{

  //-----------------------------------------------------------------------------
  /** @class MCTruthTool RichMCTruthTool.h
   *
   *  Tool performing MC truth associations
   *
   *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
   *  @date   15/03/2002
   *
   *  @todo Figure out how to best deal with sub-pixel info in MC mappings
   */
  //-----------------------------------------------------------------------------

  class MCTruthTool final : public Rich::ToolBase,
                            virtual public IMCTruthTool,
                            virtual public IIncidentListener
  {

  public: // Methods for Gaudi Framework

    /// Standard constructor
    MCTruthTool( const std::string &type, const std::string &name, const IInterface *parent );

    // Initialization of the tool after creation
    StatusCode initialize() override;

    /** Implement the handle method for the Incident service.
     *  This is used to inform the tool of software incidents.
     *
     *  @param incident The incident identifier
     */
    void handle( const Incident &incident ) override;

  public: // methods (and doxygen comments) inherited from interface

    // Find best MCParticle association for a given reconstructed Track
    const LHCb::MCParticle *mcParticle( const LHCb::Track *track,
                                        const double       minWeight = 0.5 ) const override;

    // get MCRichHits for MCParticle
    const SmartRefVector< LHCb::MCRichHit > &
    mcRichHits( const LHCb::MCParticle *mcp ) const override;

    // Get the MCRichHits associated to a given RichSmartID
    const SmartRefVector< LHCb::MCRichHit > &
    mcRichHits( const LHCb::RichSmartID smartID ) const override;

    // Get a vector of MCParticles associated to given RichSmartID
    bool mcParticles( const LHCb::RichSmartID                  id,
                      std::vector< const LHCb::MCParticle * > &mcParts ) const override;

    // Determines the particle mass hypothesis for a given MCParticle
    Rich::ParticleIDType mcParticleType( const LHCb::MCParticle *mcPart ) const override;

    // Finds the MCRichDigit association for a RichSmartID channel identifier
    const LHCb::MCRichDigit *mcRichDigit( const LHCb::RichSmartID id ) const override;

    // Finds the MCRichTrack associated to a given MCParticle
    const LHCb::MCRichTrack *mcRichTrack( const LHCb::MCParticle *mcPart ) const override;

    // Finds the MCRichOpticalPhoton associated to a given MCRichHit
    const LHCb::MCRichOpticalPhoton *mcOpticalPhoton( const LHCb::MCRichHit *mcHit ) const override;

    // Access the bit-pack history object for the given RichSmartID
    bool
    getMcHistories( const LHCb::RichSmartID                          id,
                    std::vector< const LHCb::MCRichDigitSummary * > &histories ) const override;

    // Checks if the given RichSmartID is the result of a background hit
    bool isBackground( const LHCb::RichSmartID id ) const override;

    // Checks if the given RichSmartID is the result of a photon which
    // underwent reflections inside the PD
    bool isHPDReflection( const LHCb::RichSmartID id ) const override;

    // Checks if the given RichSmartID is the result of backscattering
    // of the HPD silicon sensor
    bool isSiBackScatter( const LHCb::RichSmartID id ) const override;

    // Checks if the given RichSmartID is the result of scintillation
    // of the radiator
    bool isRadScintillation( const LHCb::RichSmartID id ) const override;

    // Checks if the given RichSmartID is the result of true Cherenkov
    bool isCherenkovRadiation( const LHCb::RichSmartID  id,
                               const Rich::RadiatorType rad ) const override;

    // Returns true if MC information for the RICH hits are available
    bool richMCHistoryAvailable() const override;

    // Checks if RICH extended MC information (MCRichOpticalPhoton, MCRichSegment etc.)
    bool extendedMCAvailable() const override;

    // Get the MCRichHits associated to a cluster of RichSmartIDs
    void mcRichHits( const Rich::PDPixelCluster &       cluster,
                     SmartRefVector< LHCb::MCRichHit > &hits ) const override;

    // Access the bit-pack history objects for the given cluster of RichSmartIDs
    bool
    getMcHistories( const Rich::PDPixelCluster &                     cluster,
                    std::vector< const LHCb::MCRichDigitSummary * > &histories ) const override;

    // Checks if the given cluster of RichSmartIDs is the result of a background
    bool isBackground( const Rich::PDPixelCluster &cluster ) const override;

    // Checks if the given cluster of RichSmartIDs is the result of a photon which
    // underwent reflections inside the PD
    bool isHPDReflection( const Rich::PDPixelCluster &cluster ) const override;

    // Checks if the given cluster of RichSmartIDs is the result of backscattering
    // of the PD silicon sensor
    bool isSiBackScatter( const Rich::PDPixelCluster &cluster ) const override;

    // Checks if the given cluster of RichSmartIDs is the result scintillation
    // of radiator
    bool isRadScintillation( const Rich::PDPixelCluster &cluster ) const override;

    // Checks if the given RichSmartID is the result of true Cherenkov
    bool isCherenkovRadiation( const Rich::PDPixelCluster &cluster,
                               const Rich::RadiatorType    rad ) const override;

    // Get a vector of MCParticles associated to given RichSmartID cluster
    bool mcParticles( const Rich::PDPixelCluster &             cluster,
                      std::vector< const LHCb::MCParticle * > &mcParts ) const override;

  private: // definitions

    /// typedef of the Linker object for MCParticles to MCRichTracks
    typedef LinkedTo< LHCb::MCRichTrack, LHCb::MCParticle > MCPartToRichTracks;

    /// typedef of the Linker object for MCRichHits to MCRichOpticalPhotons
    typedef LinkedTo< LHCb::MCRichOpticalPhoton, LHCb::MCRichHit > MCRichHitToPhoton;

    /// Typedef for vector of pointers to MCRichDigitSummaries
    typedef std::vector< const LHCb::MCRichDigitSummary * > MCRichDigitSummaries;

    /// Typedef for map between RichSmartIDs and MCRichDigitSummary objects
    typedef Rich::Map< const LHCb::RichSmartID, MCRichDigitSummaries > MCRichDigitSummaryMap;

    /// Typedef for mapping from MCParticle to MCRichHits
    typedef Rich::Map< const LHCb::MCParticle *, SmartRefVector< LHCb::MCRichHit > >
      MCPartToMCRichHits;

    /// Typedef for mapping between RichSmartIDs and MCRichHits
    typedef Rich::Map< const LHCb::RichSmartID, SmartRefVector< LHCb::MCRichHit > >
      SmartIDToMCRichHits;

    // track linker stuff
    typedef LinkerTool< LHCb::Track, LHCb::MCParticle >                 TrackToMCP;
    typedef TrackToMCP::DirectType                                      TrToMCTable;
    typedef Rich::HashMap< std::string, std::unique_ptr< TrackToMCP > > TrackLocToMCPs;

  private: // private methods

    /// Returns the linker object for Tracks to MCParticles
    TrackToMCP *trackToMCPLinks( const LHCb::Track *track = nullptr ) const;

    /// Returns the linker for the given Track location
    TrackToMCP *trackToMCPLinks( const std::string &loc ) const;

    /// Access the mapping from MCParticles to MCRichHits
    const MCPartToMCRichHits &mcPartToMCRichHitsMap() const;

    /// Access the mapping from RichSmartIDs to MCRichHits
    const SmartIDToMCRichHits &smartIDToMCRichHitsMap() const;

    /// Returns the linker object for MCParticles to MCRichTracks
    MCPartToRichTracks *mcTrackLinks() const;

    /// Returns the linker object for MCRichHits to MCRichOpticalPhotons
    MCRichHitToPhoton *mcPhotonLinks() const;

    /** Loads the MCRichDigits from the TES
     *
     * @return Pointer to the MCRichDigits
     * @retval NULL means no MC information is available
     */
    const LHCb::MCRichDigits *mcRichDigits() const;

    /** Loads the MCRichDigitSummarys from TES
     *
     * @return Pointer to the MCRichDigitSummaryVector
     * @retval NULL means no MC information is available
     */
    const LHCb::MCRichDigitSummarys *mcRichDigitSummaries() const;

    /** Loads the MCRichHits from the TES
     *
     * @return Pointer to the MCRichHits
     * @retval NULL means no MC information is available
     */
    const LHCb::MCRichHits *mcRichHits() const;

    /// Initialise for a new event
    void InitNewEvent();

    /// Access the map between RichSmartIDs and MCRichDigitSummaries
    const MCRichDigitSummaryMap &mcRichDigSumMap() const;

  private: // private data

    /// Flag to say MCRichDigits have been loaded for this event
    mutable bool m_mcRichDigitsDone { false };

    /// Flag to say MCRichDigitSummarys has been loaded for this event
    mutable bool m_mcRichDigitSumsDone { false };

    /// Flag to say mapping between RichSmartIDs and MCRichDigitSummary objects
    /// has been created for this event
    mutable bool m_mcRichDigSumMapDone { false };

    /// Flag to say MCRichHits have been loaded for this event
    mutable bool m_mcRichHitsDone { false };

    /// Pointer to MCRichDigits
    mutable LHCb::MCRichDigits *m_mcRichDigits = nullptr;

    /// Pointer to MCRichDigitSummarys
    mutable LHCb::MCRichDigitSummarys *m_mcRichDigitSums = nullptr;

    /// Pointer to MCRichDigits
    mutable LHCb::MCRichHits *m_mcRichHits = nullptr;

    /// Linker for MCParticles to MCRichTracks
    mutable std::unique_ptr< MCPartToRichTracks > m_mcTrackLinks;

    /// Linker for MCRichHits to MCRichOpticalPhotons
    mutable std::unique_ptr< MCRichHitToPhoton > m_mcPhotonLinks;

    /// mapping from MCParticles to MCRichHits
    mutable MCPartToMCRichHits m_mcPToHits;

    /// mapping for RichSmartIDs to MCRichHits
    mutable SmartIDToMCRichHits m_smartIDsToHits;

    /// Location of MCRichDigits in EDS
    std::string m_mcRichDigitsLocation;

    /// Location of MCRichDigitSummarys in EDS
    std::string m_mcRichDigitSumsLocation;

    /// Location of MCRichHits in EDS
    std::string m_mcRichHitsLocation;

    /// PID information
    Rich::Map< int, Rich::ParticleIDType > m_localID;

    /// Map between RichSmartIDs and MCRichDigitSummary objects
    mutable MCRichDigitSummaryMap m_mcRichDigSumMap;

    /// Empty container for missing links
    SmartRefVector< LHCb::MCRichHit > m_emptyContainer;

    /// Location of Tracks in TES
    Gaudi::Property< std::string > m_trLoc { this,
                                             "TrackLocation",
                                             "/Event/" + LHCb::TrackLocation::Default };

    /// Linker for Tracks to MCParticles
    mutable TrackLocToMCPs m_trToMCPLinks;
  };

  inline void MCTruthTool::InitNewEvent()
  {
    m_mcRichDigitsDone    = false;
    m_mcRichDigitSumsDone = false;
    m_mcRichHitsDone      = false;
    m_mcRichDigSumMapDone = false;
    m_mcRichDigitSums     = nullptr;
    m_mcPToHits.clear();
    m_smartIDsToHits.clear();
    m_mcTrackLinks.reset( nullptr );
    m_mcPhotonLinks.reset( nullptr );
    m_trToMCPLinks.clear();
  }

} // namespace Rich::MC
