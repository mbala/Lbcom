
//---------------------------------------------------------------------------------
/** @file RichAddBackground.cpp
 *
 * Implementation file for class : Rich::AddBackground
 *
 * @author Chris Jones   Christopher.Rob.Jones@cern.ch
 * @date   2008-10-10
 */
//---------------------------------------------------------------------------------

// local
#include "RichAddBackground.h"

//-----------------------------------------------------------------------------

using namespace Rich;

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( AddBackground )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
AddBackground::AddBackground( const std::string &name, ISvcLocator *pSvcLocator )
  : Rich::AlgBase( name, pSvcLocator )
{}

//=============================================================================
// Initialization
//=============================================================================
StatusCode
AddBackground::initialize()
{
  StatusCode sc = Rich::AlgBase::initialize();
  if ( sc.isFailure() ) return sc;

  // RichDet
  m_richSys = getDet< DeRichSystem >( DeRichLocations::RichSystem );

  // tools
  acquireTool( "RichAddBackground", m_background, this );
  acquireTool( "RichSmartIDDecoder", m_SmartIDDecoder, nullptr, true );

  return sc;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode
AddBackground::execute()
{
  StatusCode sc = StatusCode::SUCCESS;

  // ALICE or LHCb mode (to do)
  const bool aliceMode = false;

  // Create the additional background hits
  Rich::IAddBackground::HPDBackgrounds backgrounds;
  sc = m_background->createBackgrounds( backgrounds, aliceMode );

  // loop over the new backgrounds and merge them with the main decoded data
  for ( const auto &B : backgrounds )
  {
    _ri_debug << "Found " << B.second.size() << " background hits for " << B.first << endmsg;
    counter( "Added background hits / HPD" ) += B.second.size();

    auto &smartIDs = *( const_cast< LHCb::RichSmartID::Vector * >(
      &( m_SmartIDDecoder->richSmartIDs( B.first, true ) ) ) );

    _ri_debug << "  -> Found " << smartIDs.size() << " pre-existing hits" << endmsg;

    // add the hits
    for ( const auto &BHit : B.second )
    {
      // Is this hit already in the list ? (binary so should only be there once)
      if ( smartIDs.end() == std::find( smartIDs.begin(), smartIDs.end(), BHit ) )
      { smartIDs.emplace_back( BHit ); }
    }

    _ri_debug << "  ->       " << smartIDs.size() << " hits after background addition" << endmsg;
  }

  return sc;
}

//=============================================================================
