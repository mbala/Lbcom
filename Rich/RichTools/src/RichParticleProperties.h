
//-----------------------------------------------------------------------------
/** @file RichParticleProperties.h
 *
 *  Header file for tool : Rich::ParticleProperties
 *
 *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date   15/03/2002
 */
//-----------------------------------------------------------------------------

#pragma once

// STL
#include <array>
#include <cmath>
#include <vector>

// Utils
#include "RichUtils/RichTrackSegment.h"

// base class
#include "RichKernel/RichToolBase.h"

// interfaces
#include "RichInterfaces/IRichParticleProperties.h"
#include "RichInterfaces/IRichRefractiveIndex.h"

// boost
#include "boost/limits.hpp"
#include "boost/numeric/conversion/bounds.hpp"

namespace Rich
{

  //-----------------------------------------------------------------------------
  /** @class ParticleProperties RichParticleProperties.h
   *
   *  Tool to calculate various physical properties
   *  for the different mass hypotheses.
   *
   *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
   *  @date   15/03/2002
   */
  //-----------------------------------------------------------------------------

  class ParticleProperties final : public Rich::ToolBase, virtual public IParticleProperties
  {

  public: // Methods for Gaudi Framework

    /// Standard constructor
    ParticleProperties( const std::string &type,
                        const std::string &name,
                        const IInterface * parent );

    // Initialize method
    StatusCode initialize() override;

  public: // methods (and doxygen comments) inherited from public interface

    // Returns 'beta' for given particle hypothesis
    ScType beta( const ScType ptot, const Rich::ParticleIDType id ) const override;

    // Returns the nominal mass for a given particle type
    ScType mass( const Rich::ParticleIDType id ) const override;

    // Returns the nominal mass squared for a given particle type
    ScType massSq( const Rich::ParticleIDType id ) const override;

    // Returns the threshold momentum for a given hypothesis in a given radiator
    ScType thresholdMomentum( const Rich::ParticleIDType id,
                              const Rich::RadiatorType   rad ) const override;

    // Calculates the threshold momentum for a given mass hypothesis
    ScType thresholdMomentum( const Rich::ParticleIDType    id,
                              const LHCb::RichTrackSegment &trSeg ) const override;

    // Vector of the mass hypotheses to be considered
    const Rich::Particles &particleTypes() const override;

  private: // Private data

    /// Refractive index tool
    const IRefractiveIndex *m_refIndex = nullptr;

    /// Array containing particle masses
    Rich::ParticleArray< ScType > m_particleMass = { {} };

    /// Array containing square of particle masses
    Rich::ParticleArray< ScType > m_particleMassSq = { {} };

    /// Particle ID types to consider in the likelihood minimisation (JO)
    std::vector< std::string > m_pidTypesJO;

    /// Particle ID types to consider in the likelihood minimisation
    Rich::Particles m_pidTypes { Rich::particles() };
  };

} // namespace Rich
