
//-----------------------------------------------------------------------------
/** @file RichHPDPixelClusteringTool.cpp
 *
 * Implementation file for class : RichHPDPixelClusteringTool
 *
 * @author Chris Jones   Christopher.Rob.Jones@cern.ch
 * @date   21/03/2006
 */
//-----------------------------------------------------------------------------

// local
#include "RichHPDPixelClusteringTool.h"

// RICH DAQ
using namespace Rich::DAQ;

//-----------------------------------------------------------------------------

// Standard constructor
HPDPixelClusteringTool::HPDPixelClusteringTool( const std::string &type,
                                                const std::string &name,
                                                const IInterface * parent )
  : ToolBase( type, name, parent )
{
  // Define interface
  declareInterface< IPixelClusteringTool >( this );
  // job options
  declareProperty( "MinClusterSize", m_minClusSize = 1 );
  declareProperty( "MaxClusterSize", m_maxClusSize = 999999 );
  declareProperty( "AllowDiagonalsInClusters", m_allowDiags = true );
  // setProperty( "OutputLevel", 1 );
}

StatusCode
HPDPixelClusteringTool::initialize()
{
  // Sets up various tools and services
  const StatusCode sc = ToolBase::initialize();
  if ( sc.isFailure() ) return sc;

  m_splitClusters = ( m_minClusSize > 1 || m_maxClusSize < 999999 );

  _ri_debug << "Using nearest neighbour pixel clustering tool" << endmsg
            << " -> Minimum cluster size        = " << m_minClusSize << endmsg
            << " -> Maximum cluster size        = " << m_maxClusSize << endmsg
            << " -> Allow diagonals in clusters = " << m_allowDiags << endmsg;

  return sc;
}

std::unique_ptr< const Rich::PDPixelClusters >
HPDPixelClusteringTool::findClusters( const LHCb::RichSmartID::Vector &smartIDs ) const
{
  // make sure pixels are sorted ok
  // this should be automatic via decoding but can do it here to be sure
  // as because the vector should be in the correct order, it should be v fast
  sortIDs( smartIDs );

  // Make a new pixel clusters object to return
  // auto * pixelData = new PDPixelClusters();

  // Pixel clusters builder
  HPDPixelClustersBuilder clusterBuilder;

  // Initialise the builder
  clusterBuilder.initialise( smartIDs );
  auto &pixelData = clusterBuilder.data();

  // loop over pixels
  // requires them to be sorted by row then column
  _ri_verbo << "Clustering with " << smartIDs.size() << " RichSmartIDs" << endmsg;
  for ( const auto &S : smartIDs )
  {
    // Print the input hits
    _ri_verbo << " -> " << S << endmsg;

    // get row and column data
    const auto col     = clusterBuilder.colNumber( S );
    const auto row     = clusterBuilder.rowNumber( S );
    const auto lastrow = row - 1;
    const auto lastcol = col - 1;
    const auto nextcol = col + 1;

    // Null cluster pointer
    PDPixelClusters::Cluster *clus( nullptr );

    // check neighbouring pixels

    // last row and last column
    clus = ( m_allowDiags ? clusterBuilder.getCluster( lastrow, lastcol ) : nullptr );

    // last row and same column
    {
      auto *newclus1 = clusterBuilder.getCluster( lastrow, col );
      if ( newclus1 )
      {
        clus =
          ( clus && clus != newclus1 ? clusterBuilder.mergeClusters( clus, newclus1 ) : newclus1 );
      }
    }

    // last row and next column
    {
      auto *newclus2 = ( m_allowDiags ? clusterBuilder.getCluster( lastrow, nextcol ) : nullptr );
      if ( newclus2 )
      {
        clus =
          ( clus && clus != newclus2 ? clusterBuilder.mergeClusters( clus, newclus2 ) : newclus2 );
      }
    }

    // this row and last column
    {
      auto *newclus3 = clusterBuilder.getCluster( row, lastcol );
      if ( newclus3 )
      {
        clus =
          ( clus && clus != newclus3 ? clusterBuilder.mergeClusters( clus, newclus3 ) : newclus3 );
      }
    }

    // Did we find a neighbouring pixel cluster ?
    // If not, this is a new cluster
    if ( !clus ) { clus = clusterBuilder.createNewCluster(); }

    // assign final cluster to this pixel
    clusterBuilder.setCluster( S, row, col, clus );

  } // pixel loop

  // Check final clusters are OK
  if ( UNLIKELY( m_splitClusters ) )
  {
    // make a local working object
    PDPixelClusters::Cluster::PtnVector clustersToSplit;
    for ( auto &C : pixelData.clusters() )
    {
      if ( C.size() < m_minClusSize || C.size() > m_maxClusSize )
      { clustersToSplit.push_back( &C ); }
    }
    if ( !clustersToSplit.empty() )
    {
      // split the selected clusters
      clusterBuilder.splitClusters( clustersToSplit );
    }
  }

  // print the final cluster
  //_ri_verbo << m_clusterBuilder << endmsg;
  //_ri_verbo << *pixelData       << endmsg;

  // move clusters here to new return object as no longer need builder
  return std::make_unique< const Rich::PDPixelClusters >( std::move( pixelData ) );
}

//-----------------------------------------------------------------------------

DECLARE_COMPONENT( HPDPixelClusteringTool )

//-----------------------------------------------------------------------------
