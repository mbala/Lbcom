
//-----------------------------------------------------------------------------
/** @file RichTabulatedRefractiveIndex.cpp
 *
 *  Implementation file for class : RichTabulatedRefractiveIndex
 *
 *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date 15/03/2002
 */
//-----------------------------------------------------------------------------

// local
#include "RichTabulatedRefractiveIndex.h"

// From Gaudi
#include "GaudiKernel/SystemOfUnits.h"

DECLARE_COMPONENT( Rich::TabulatedRefractiveIndex )

// Standard constructor
Rich::TabulatedRefractiveIndex::TabulatedRefractiveIndex( const std::string &type,
                                                          const std::string &name,
                                                          const IInterface * parent )
  : Rich::ToolBase( type, name, parent )
{
  // Initialise arrays
  m_riches.fill( nullptr );
  m_radiators.fill( nullptr );
  // interface
  declareInterface< IRefractiveIndex >( this );
  // JOs
  declareProperty( "ForceHLTMode", m_forceHltMode = true );
}

StatusCode
Rich::TabulatedRefractiveIndex::initialize()
{
  // Initialise base class
  const StatusCode sc = Rich::ToolBase::initialize();
  if ( sc.isFailure() ) return sc;

  // Get tools
  acquireTool( "RichDetParameters", m_detParams, nullptr, true );

  // Rich1 and Rich2
  m_riches[ Rich::Rich1 ] = getDet< DeRich1 >( DeRichLocations::Rich1 );
  m_riches[ Rich::Rich2 ] = getDet< DeRich2 >( DeRichLocations::Rich2 );

  // HLT mode or not ?
  m_hltMode = ( m_forceHltMode || contextContains( "HLT" ) );

  return sc;
}

Rich::IRefractiveIndex::ScType
Rich::TabulatedRefractiveIndex::refractiveIndex( const Rich::RadiatorType rad,
                                                 const ScType             energy ) const
{
  return deRad( rad )->refractiveIndex( energy, m_hltMode );
}

Rich::IRefractiveIndex::ScType
Rich::TabulatedRefractiveIndex::refractiveIndex( const Rich::RadiatorType rad,
                                                 const ScType             energyBot,
                                                 const ScType             energyTop ) const
{
  const auto rich = ( rad == Rich::Rich2Gas ? Rich::Rich2 : Rich::Rich1 );
  return refractiveIndex( rad,
                          m_riches[ rich ]->nominalPDQuantumEff()->meanX( energyBot, energyTop ) /
                            Gaudi::Units::eV );
}

Rich::IRefractiveIndex::ScType
Rich::TabulatedRefractiveIndex::refractiveIndex( const Rich::RadiatorType rad ) const
{
  return refractiveIndex( rad, m_detParams->meanPhotonEnergy( rad ) );
}

Rich::IRefractiveIndex::ScType
Rich::TabulatedRefractiveIndex::refractiveIndex( const Rich::RadIntersection::Vector &intersections,
                                                 const ScType                         energy ) const
{
  // loop over all radiator intersections and calculate the weighted average
  // according to the path length in each radiator
  ScType refIndex( 0 ), totPathL( 0 );
  for ( const auto &R : intersections )
  {
    const auto pLength = R.pathLength();
    refIndex += pLength * R.radiator()->refractiveIndex( energy, m_hltMode );
    totPathL += pLength;
  }
  return ( totPathL > 0 ? refIndex / totPathL : refIndex );
}

Rich::IRefractiveIndex::ScType
Rich::TabulatedRefractiveIndex::refractiveIndex(
  const Rich::RadIntersection::Vector &intersections ) const
{
  // loop over all radiator intersections and calculate the weighted average
  // according to the path length in each radiator
  ScType refIndex( 0 ), totPathL( 0 );
  for ( const auto &R : intersections )
  {
    const auto energy  = m_detParams->meanPhotonEnergy( R.radiator()->radiatorID() );
    const auto pLength = R.pathLength();
    refIndex += pLength * R.radiator()->refractiveIndex( energy, m_hltMode );
    totPathL += pLength;
  }
  return ( totPathL > 0 ? refIndex / totPathL : refIndex );
}

Rich::IRefractiveIndex::ScType
Rich::TabulatedRefractiveIndex::refractiveIndexRMS(
  const Rich::RadIntersection::Vector &intersections ) const
{
  // loop over all radiator intersections and calculate the weighted average
  // according to the path length in each radiator
  ScType refIndexRMS( 0 ), totPathL( 0 );
  for ( const auto &R : intersections )
  {
    const auto  pLength = R.pathLength();
    const auto *index   = R.radiator()->refIndex( m_hltMode );
    refIndexRMS += pLength * index->rms( index->minX(), index->maxX(), 100 );
    totPathL += pLength;
  }
  return ( totPathL > 0 ? refIndexRMS / totPathL : refIndexRMS );
}

Rich::IRefractiveIndex::ScType
Rich::TabulatedRefractiveIndex::refractiveIndexSD(
  const Rich::RadIntersection::Vector &intersections ) const
{
  // loop over all radiator intersections and calculate the weighted average
  // according to the path length in each radiator
  ScType refIndexSD( 0 ), totPathL( 0 );
  for ( const auto &R : intersections )
  {
    const auto  pLength = R.pathLength();
    const auto *index   = R.radiator()->refIndex( m_hltMode );
    refIndexSD += pLength * index->standardDeviation( index->minX(), index->maxX(), 100 );
    totPathL += pLength;
  }
  return ( totPathL > 0 ? refIndexSD / totPathL : refIndexSD );
}
