
// local
#include "RichSmartIDClustering.h"

using namespace Rich::Future;

//-----------------------------------------------------------------------------
// Implementation file for class : RichSmartIDClustering
//
// 2016-09-28 : Chris Jones
//-----------------------------------------------------------------------------

SmartIDClustering::SmartIDClustering( const std::string &name, ISvcLocator *pSvcLocator )
  : Transformer(
      name,
      pSvcLocator,
      { KeyValue { "DecodedDataLocation", Rich::Future::DAQ::L1MapLocation::Default } },
      { KeyValue { "RichPixelClustersLocation", Rich::PDPixelClusterLocation::Default } } )
{
  // debug
  // setProperty( "OutputLevel", MSG::VERBOSE );
}

//=============================================================================

StatusCode
SmartIDClustering::initialize()
{
  // Initialise base class
  const auto sc = Transformer::initialize();
  if ( !sc ) return sc;

  // Load the RICH detector system
  m_richSys = getDet< DeRichSystem >( DeRichLocations::RichSystem );

  // Warnings
  for ( const auto rich : { Rich::Rich1, Rich::Rich2 } )
  {
    if ( !m_usedDets[ rich ] )
    {
      Warning( "Pixels for " + Rich::text( rich ) + " are disabled", StatusCode::SUCCESS ).ignore();
    }
  }

  return sc;
}

//=============================================================================

Rich::PDPixelCluster::Vector
SmartIDClustering::operator()( const Rich::Future::DAQ::L1Map &data ) const
{
  // The clusters to fill and return
  Rich::PDPixelCluster::Vector clusters;
  clusters.reserve( data.nTotalHits() );

  // cluster builder. Allocated as required.
  std::unique_ptr< PDPixelClustersBuilder > clusterBuilder;

  // Working container of pointers to decoded HPD data objects
  std::vector< const Rich::Future::DAQ::PDInfo * > pdDataV;
  pdDataV.reserve( data.nActivePDs() );

  // Loop over the raw RICH data and select good PDs
  for ( const auto &L1 : data )
  {
    // loop over ingresses for this L1 board
    for ( const auto &In : L1.second )
    {
      // Loop over HPDs in this ingress
      for ( const auto &PD : In.second.pdData() )
      {
        // Which RICH
        const auto rich = PD.second.pdID().rich();
        // The list of hits
        const auto &IDs = PD.second.smartIDs();
        // Is it selected ?
        if ( !PD.second.header().inhibit() &&     // PD not inhibited
             pdIsOK( PD.second.pdID() ) &&        // PD ID is OK
             !IDs.empty() &&                      // PD has some hits
             IDs.size() <= m_overallMax[ rich ] ) // PD max occ cut
        {
          // Save this PD
          pdDataV.emplace_back( &PD.second );
        }
      }
    }
  }

  // Sort PD data pointer vector by region.
  // Faster than sorting final cluster container
  std::sort( pdDataV.begin(), pdDataV.end(), []( const auto a, const auto b ) {
    return ( a->pdID().key() < b->pdID().key() );
  } );

  // Vector for sorted IDs, used later on.
  // Defined here so can be shared across different PDs.
  LHCb::RichSmartID::Vector smartIDs;

  // Loop over the PD vector and cluster
  for ( const auto PD : pdDataV )
  {

    // Which RICH
    const auto rich = PD->pdID().rich();

    // Which panel
    const auto panel = PD->pdID().panel();

    // Get the DePD pointer for this PD
    const auto dePD = m_richSys->dePD( PD->pdID() );

    // is cluster finding active and required (i.e. more than 1 hit) ?
    if ( !m_clusterHits[ rich ] || PD->smartIDs().size() < 2 )
    {

      // just loop over the raw RichSmartIDs and make a 'cluster' for each
      for ( const auto &ID : PD->smartIDs() ) { clusters.emplace_back( rich, panel, ID, dePD ); }
    }
    else
    {
      // perform clustering on the pixels in this PD

      // Need to make a copy of the hits, as need to sort them and
      // we cannot change the raw data.
      smartIDs = PD->smartIDs();

      // make sure pixels are sorted ok
      // this should be automatic via decoding but can do it here to be sure
      // as because the vector should be in the correct order, it should be v fast
      sortIDs( smartIDs );

      // initialise the builder
      if ( UNLIKELY( !clusterBuilder.get() ) )
      {
        // Do we need a PMT or HPD builder ?
        if ( LHCb::RichSmartID::IDType::MaPMTID == PD->pdID().idType() )
        { clusterBuilder = std::make_unique< PMTPixelClustersBuilder >(); }
        else
        {
          clusterBuilder = std::make_unique< HPDPixelClustersBuilder >();
        }
      }
      clusterBuilder->initialise( smartIDs );

      // loop over pixels
      // requires them to be sorted by row then column
      _ri_verbo << "Clustering with " << smartIDs.size() << " RichSmartIDs" << endmsg;
      for ( const auto &S : smartIDs )
      {
        // Print the input hits
        _ri_verbo << " -> " << S << endmsg;

        // get row and column data
        const auto col     = clusterBuilder->colNumber( S );
        const auto row     = clusterBuilder->rowNumber( S );
        const auto lastrow = row - 1;
        const auto lastcol = col - 1;
        const auto nextcol = col + 1;

        // Null cluster pointer
        PDPixelClusters::Cluster *clus( nullptr );

        // check neighbouring pixels

        // last row and last column
        clus = ( m_allowDiags[ rich ] ? clusterBuilder->getCluster( lastrow, lastcol ) : nullptr );

        // last row and same column
        {
          auto newclus1 = clusterBuilder->getCluster( lastrow, col );
          if ( newclus1 && newclus1->size() < m_maxClusSize[ rich ] )
          {
            clus = ( clus && clus != newclus1 &&
                         ( newclus1->size() + clus->size() ) < m_maxClusSize[ rich ] ?
                       clusterBuilder->mergeClusters( clus, newclus1 ) :
                       newclus1 );
          }
        }

        // last row and next column
        {
          auto newclus2 =
            ( m_allowDiags[ rich ] ? clusterBuilder->getCluster( lastrow, nextcol ) : nullptr );
          if ( newclus2 && newclus2->size() < m_maxClusSize[ rich ] )
          {
            clus = ( clus && clus != newclus2 &&
                         ( newclus2->size() + clus->size() ) < m_maxClusSize[ rich ] ?
                       clusterBuilder->mergeClusters( clus, newclus2 ) :
                       newclus2 );
          }
        }

        // this row and last column
        {
          auto newclus3 = clusterBuilder->getCluster( row, lastcol );
          if ( newclus3 && newclus3->size() < m_maxClusSize[ rich ] )
          {
            clus = ( clus && clus != newclus3 &&
                         ( newclus3->size() + clus->size() ) < m_maxClusSize[ rich ] ?
                       clusterBuilder->mergeClusters( clus, newclus3 ) :
                       newclus3 );
          }
        }

        // Did we find a neighbouring pixel cluster ?
        // If not, this is a new cluster
        if ( !clus ) { clus = clusterBuilder->createNewCluster(); }

        // assign final cluster to this pixel
        clusterBuilder->setCluster( S, row, col, clus );

      } // pixel loop

      // the pixel data object filled by the builder
      auto &pixelData = clusterBuilder->data();

      // Do we need to break up clusters ?
      if ( UNLIKELY( m_minClusSize[ rich ] > 1u ) )
      {
        // make a local working object
        PDPixelClusters::Cluster::PtnVector clustersToSplit;
        for ( auto &C : pixelData.clusters() )
        {
          if ( C.size() < m_minClusSize[ rich ] || C.size() > m_maxClusSize[ rich ] )
          { clustersToSplit.push_back( &C ); }
        }
        if ( !clustersToSplit.empty() )
        {
          // split the selected clusters
          clusterBuilder->splitClusters( clustersToSplit );
        }
      }

      // Print the cluster results
      _ri_verbo << *( clusterBuilder.get() ) << endmsg;

      // Note : Using move from here on to move the data from the builder

      // loop over the final clusters and save them
      for ( auto &clus : pixelData.clusters() )
      {
        // Skip empty clusters
        if ( !clus.pixels().empty() )
        {
          // If we have no secondary pixels, use optimised constructor.
          if ( clus.pixels().secondaryIDs().empty() )
          { clusters.emplace_back( rich, panel, clus.pixels().primaryID(), dePD ); }
          // Use the move constructor
          else
          {
            clusters.emplace_back( std::move( clus.pixels() ) );
            clusters.back().setDePD( dePD );
          }
        }
      }

    } // run clustering

  } // PD loop

  // GEC cut
  if ( UNLIKELY( clusters.size() > m_maxClusters ) )
  {
    // Too many hits. Clear to prevent event processing.
    clusters.clear();
    Warning( "Too many clusters (>" + std::to_string( m_maxClusters.value() ) +
               "). Processing aborted.",
             StatusCode::SUCCESS,
             0 )
      .ignore();
  }

  // return the found clusters
  return clusters;
}

//=============================================================================

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( SmartIDClustering )

//=============================================================================
