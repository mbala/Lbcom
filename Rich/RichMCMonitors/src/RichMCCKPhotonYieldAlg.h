
//-----------------------------------------------------------------------------
/** @file RichMCCKPhotonYieldAlg.h
 *
 * Header file for monitor algorithm Rich::MC::MCCKPhotonYieldAlg
 *
 * @author Chris Jones   Christopher.Rob.Jones@cern.ch
 * @date 2006-11-03
 */
//-----------------------------------------------------------------------------

#pragma once

// STL
#include <cmath>
#include <vector>

// base class
#include "RichKernel/RichHistoAlgBase.h"

// MCEvent
#include "Event/MCRichHit.h"
#include "Event/MCRichTrack.h"

// Rich Utils
#include "RichUtils/RichMap.h"
#include "RichUtils/RichStatDivFunctor.h"

// tool Interfaces
#include "MCInterfaces/IRichMCTruthTool.h"

// boost
#include "boost/assign/list_of.hpp"

namespace Rich::MC
{

  //-----------------------------------------------------------------------------
  /** @class MCCKPhotonYieldAlg RichMCCKPhotonYieldAlg.h
   *
   *  Monitor algorithm to study the RICH CK photon yield using
   *  only MCRichHit and MCParticle data objects
   *
   *  @author Chris Jones  Christopher.Rob.Jones@cern.ch
   *  @date   2006-11-03
   */
  //-----------------------------------------------------------------------------

  class MCCKPhotonYieldAlg final : public Rich::HistoAlgBase
  {

  public:

    /// Standard constructor
    MCCKPhotonYieldAlg( const std::string &name, ISvcLocator *pSvcLocator );

    StatusCode execute() override;  ///< Algorithm execution
    StatusCode finalize() override; ///< Algorithm finalization

  private: // utility classes

    /// Tally class to store the number of photons for a number of tracks
    class TrackPhotonTally
    {
    public:

      /// Default constructor
      TrackPhotonTally() = default;
      /// Constructor with optional number of photons and tracks
      TrackPhotonTally( const unsigned long long tracks, const unsigned long long photons )
        : nTracks( tracks ), nPhotons( photons )
      {}

    public:

      unsigned long long nTracks { 0 };  ///< tally for the number of tracks
      unsigned long long nPhotons { 0 }; ///< tally for the number of photons
    };

  private: // definitions

    /// A vector of pointers to MCRichHits
    typedef std::vector< const LHCb::MCRichHit * > MCRichHitVector;
    /// MCParticle/radiator key
    typedef std::pair< const LHCb::MCParticle *, Rich::RadiatorType > MCPRadKey;
    /// Mapping from MCParticles to MCRichHits
    typedef Rich::Map< MCPRadKey, MCRichHitVector > MCParticle2RichHits;
    /// Tally for number of hits in each radiator
    typedef Rich::Map< Rich::RadiatorType, TrackPhotonTally > RichRadCount;

  private: // methods

    /// Access the MC Truth Tool on-demand
    inline const Rich::MC::IMCTruthTool *mcTruthTool() const
    {
      if ( !m_mcTruth ) { acquireTool( "RichMCTruthTool", m_mcTruth, nullptr, true ); }
      return m_mcTruth;
    }

  private: // data

    /// Pointer to MCtruth association tool
    mutable const Rich::MC::IMCTruthTool *m_mcTruth = nullptr;

    /// Location of MCRichHits to analyse
    std::string m_mcRichHitsLoc;

    // selection cuts
    std::vector< double > m_minP; ///< Min momentum per radiator
    std::vector< double > m_maxP; ///< Max momentum per radiator

    std::vector< double > m_minEntryR; ///< Minimum radiator entry R ( (x*x+y*y)^0.5 )
    std::vector< double > m_minExitR;  ///< Minimum radiator exit R ( (x*x+y*y)^0.5 )
    std::vector< double > m_minEntryX; ///< Minimum radiator entry X
    std::vector< double > m_minExitX;  ///< Minimum radiator exit X
    std::vector< double > m_minEntryY; ///< Minimum radiator entry X
    std::vector< double > m_minExitY;  ///< Minimum radiator exit X

    std::vector< double > m_maxEntryR; ///< Maximum radiator entry R ( (x*x+y*y)^0.5 )
    std::vector< double > m_maxExitR;  ///< Maximum radiator exit R ( (x*x+y*y)^0.5 )
    std::vector< double > m_maxEntryX; ///< Maximum radiator entry X
    std::vector< double > m_maxExitX;  ///< Maximum radiator exit X
    std::vector< double > m_maxEntryY; ///< Maximum radiator entry X
    std::vector< double > m_maxExitY;  ///< Maximum radiator exit X

    std::vector< double > m_minPathLength; ///< Minimum path length in the radiator volume
    std::vector< double > m_maxPathLength; ///< Maximum path length in the radiator volume

    std::vector< double > m_minMRAD; ///< Minimum radiator slope (mrad)
    std::vector< double > m_maxMRAD; ///< Maximum radiator slope (mrad)

    // Track and photon tally for all events
    RichRadCount m_signalRadHits;
  };

} // namespace Rich::MC
