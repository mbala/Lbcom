//-----------------------------------------------------------------------------
/** @file RichRayTracing.h
 *
 *  Header file for tool : Rich::RayTracing
 *
 *  @author Chris Jones    Christopher.Rob.Jones@cern.ch
 *  @date   2004-03-29
 */
//-----------------------------------------------------------------------------

#pragma once

// STL
#include <array>
#include <cmath>
#include <iomanip>
#include <vector>

// Gaudi ( must be first ...)
#include "GaudiKernel/ParsersFactory.h"
#include "GaudiKernel/StdArrayAsProperty.h"

// Base class
#include "RichFutureKernel/RichToolBase.h"

// interfaces
#include "RichFutureInterfaces/IRichRayTracing.h"
#include "RichInterfaces/IRichMirrorSegFinderLookUpTable.h"
#include "RichInterfaces/IRichSnellsLawRefraction.h"

// Utils
#include "RichFutureUtils/RichGeomPhoton.h"
#include "RichFutureUtils/RichSIMDMirrorData.h"
#include "RichUtils/BoostArray.h"
#include "RichUtils/RichRayTracingUtils.h"
#include "RichUtils/RichSIMDTypes.h"
#include "RichUtils/RichTrackSegment.h"
#include "RichUtils/ZipRange.h"

// Kernel
#include "Kernel/RichSide.h"
#include "Kernel/RichSmartID.h"
#include "Kernel/RichTraceMode.h"

// RichDet
#include "RichDet/DeRich.h"
#include "RichDet/DeRichBeamPipe.h"
#include "RichDet/DeRichPDPanel.h"
#include "RichDet/DeRichSphMirror.h"
#include "RichDet/Rich1DTabProperty.h"

// Units
#include "GaudiKernel/SystemOfUnits.h"

// geometry
#include "GaudiKernel/Point3DTypes.h"
#include "GaudiKernel/Transform3DTypes.h"
#include "GaudiKernel/Vector3DTypes.h"

namespace Rich::Future
{

  //-----------------------------------------------------------------------------
  /** @class RayTracing RichRayTracing.h
   *
   *  Rich detector tool which traces photons to the photodetectors.
   *
   *  Mirror segmentation is taken into account.
   *
   *  @author Antonis Papanestis
   *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
   *  @date   2003-11-04
   *
   *  @todo Check if it is neccessary to check for intersections other than those
   *  from emission point to spherical mirror reflection point ?
   */
  //-----------------------------------------------------------------------------

  class RayTracing final : public ToolBase, virtual public IRayTracing
  {

  public: // Methods for Gaudi Framework

    /// Standard constructor
    RayTracing( const std::string &type, const std::string &name, const IInterface *parent );

    // Initialization of the tool after creation
    virtual StatusCode initialize() override;

  public: // vector methods

    /// For a given detector, ray-traces a given set of directions from a given point to
    /// the photo detectors.
    Result::Vector
    traceToDetector( const Gaudi::XYZPoint &         startPoint,
                     SIMD::STDVector< SIMDVector > &&startDirs,
                     const LHCb::RichTrackSegment &  trSeg,
                     const LHCb::RichTraceMode       mode = LHCb::RichTraceMode() ) const override;

  public: // scalar methods

    /// For a given detector, ray-traces a given direction from a given point to
    /// the photo detectors. Returns the result in the form of a RichGeomPhoton
    /// which contains the full ray tracing information.
    LHCb::RichTraceMode::RayTraceResult
    traceToDetector( const Rich::DetectorType      rich,
                     const Gaudi::XYZPoint &       startPoint,
                     const Gaudi::XYZVector &      startDir,
                     GeomPhoton &                  photon,
                     const LHCb::RichTrackSegment &trSeg,
                     const LHCb::RichTraceMode     mode       = LHCb::RichTraceMode(),
                     const Rich::Side              forcedSide = Rich::top ) const override;

    /// For a given detector, raytraces a given direction from a given point to
    /// the photo detectors. Returns the result in the form of a RichGeomPhoton.
    LHCb::RichTraceMode::RayTraceResult
    traceToDetector( const Rich::DetectorType      rich,
                     const Gaudi::XYZPoint &       startPoint,
                     const Gaudi::XYZVector &      startDir,
                     Gaudi::XYZPoint &             hitPosition,
                     const LHCb::RichTrackSegment &trSeg,
                     const LHCb::RichTraceMode     mode       = LHCb::RichTraceMode(),
                     const Rich::Side              forcedSide = Rich::top ) const override;

    /// For a given detector, raytraces a given direction from a given point to
    /// the photo detectors. Returns the result in the form of a RichGeomPhoton
    LHCb::RichTraceMode::RayTraceResult
    traceToDetector( const Rich::DetectorType  rich,
                     const Gaudi::XYZPoint &   startPoint,
                     const Gaudi::XYZVector &  startDir,
                     GeomPhoton &              photon,
                     const LHCb::RichTraceMode mode         = LHCb::RichTraceMode(),
                     const Rich::Side          forcedSide   = Rich::top,
                     const double              photonEnergy = 0 ) const override;

    /// For a given detector, raytraces a given direction from a given point to
    /// the photo detectors.
    LHCb::RichTraceMode::RayTraceResult
    traceToDetector( const Rich::DetectorType  rich,
                     const Gaudi::XYZPoint &   startPoint,
                     const Gaudi::XYZVector &  startDir,
                     Gaudi::XYZPoint &         hitPosition,
                     const LHCb::RichTraceMode mode         = LHCb::RichTraceMode(),
                     const Rich::Side          forcedSide   = Rich::top,
                     const double              photonEnergy = 0 ) const override;

    /// Raytraces from a point in the detector panel back to the spherical mirror
    /// returning the mirror intersection point and the direction a track would
    /// have in order to hit that point in the detector panel.
    bool traceBackFromDetector( const Gaudi::XYZPoint & startPoint,
                                const Gaudi::XYZVector &startDir,
                                Gaudi::XYZPoint &       endPoint,
                                Gaudi::XYZVector &      endDir ) const override;

  private: // methods

    /// Do the ray tracing
    LHCb::RichTraceMode::RayTraceResult _traceToDetector( const Rich::DetectorType  rich,
                                                          const Gaudi::XYZPoint &   startPoint,
                                                          Gaudi::XYZPoint &         tmpPos,
                                                          Gaudi::XYZVector &        tmpDir,
                                                          GeomPhoton &              photon,
                                                          const LHCb::RichTraceMode mode,
                                                          const Rich::Side forcedSide ) const;

    /// Ray trace from given position in given direction off both mirrors
    bool reflectBothMirrors( const Rich::DetectorType  rich,
                             Gaudi::XYZPoint &         position,
                             Gaudi::XYZVector &        direction,
                             GeomPhoton &              photon,
                             const LHCb::RichTraceMode mode,
                             const Rich::Side          fSide ) const;

    /// Truncate the give value to a given d.p.
    template < typename TYPE >
    inline TYPE truncate( const TYPE x ) const noexcept
    {
      // constexpr int scale = 100; // 2 d.p.
      // const long long int i = std::round( x * scale );
      // const long long int i = ( x * scale );
      // return (TYPE)(i)/scale;
      // no truncation ...
      return x;
    }

  private: // data

    /// Mirror segment finder tool
    ToolHandle< const IMirrorSegFinderLookUpTable > m_mirrorSegFinder {
      "Rich::Future::MirrorSegFinderLookUpTable/MirrorFinder:PUBLIC",
      this
    };

    /// Rich1 and Rich2 pointers
    Rich::DetectorArray< const DeRich * > m_rich { { nullptr, nullptr } };

    /// RICH beampipe object for each RICH detector
    Rich::DetectorArray< const DeRichBeamPipe * > m_deBeam { { nullptr, nullptr } };

    /// photodetector panels per rich
    using PDPanelsPerRich = std::vector< const DeRichPDPanel * >;

    /// typedef for photodetector for each rich
    using RichPDPanels = std::vector< PDPanelsPerRich >;

    /// photodetector for each rich
    RichPDPanels m_photoDetPanels;

    /// Number of primary mirror rows in each RICH
    std::array< int, Rich::NRiches > m_sphMirrorSegRows { { 0, 0 } };
    /// Number of primary mirror columns in each RICH
    std::array< int, Rich::NRiches > m_sphMirrorSegCols { { 0, 0 } };
    /// Number of secondary mirror rows in each RICH
    std::array< int, Rich::NRiches > m_secMirrorSegRows { { 0, 0 } };
    /// Number of secondary mirror columns in each RICH
    std::array< int, Rich::NRiches > m_secMirrorSegCols { { 0, 0 } };

    /// Flag to to ignore secondary mirrors (useful for test beam work)
    Gaudi::Property< bool > m_ignoreSecMirrs { this, "IgnoreSecondaryMirrors", false };

    /// Flag to control if the secondary mirrors are treated as if they are completely flat
    Gaudi::Property< DetectorArray< bool > > m_treatSecMirrsFlat { this,
                                                                   "AssumeFlatSecondaryMirrors",
                                                                   { true, false } };
  };

} // namespace Rich::Future
