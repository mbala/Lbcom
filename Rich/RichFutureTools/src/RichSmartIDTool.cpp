
//-----------------------------------------------------------------------------
/** @file RichSmartIDTool.cpp
 *
 * Implementation file for class : Rich::SmartIDTool
 *
 * @author Antonis Papanestis
 * @author Chris Jones   Christopher.Rob.Jones@cern.ch
 * @date 2003-10-28
 */
//-----------------------------------------------------------------------------

// local
#include "RichSmartIDTool.h"

using namespace Rich::Future;

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
SmartIDTool::SmartIDTool( const std::string &type,
                          const std::string &name,
                          const IInterface * parent )
  : ToolBase( type, name, parent )
{
  // Interface
  declareInterface< ISmartIDTool >( this );
  // declareProperty( "DetectorTool", m_deRichTool );
  // setProperty( "OutputLevel", 1 );
}

//=============================================================================
StatusCode
SmartIDTool::initialize()
{
  // Initialise base class
  auto sc = ToolBase::initialize();
  if ( !sc ) return sc;

  if ( sc ) sc = m_deRichTool.retrieve();

  // Get the Riches and Rich System
  const auto deRiches = m_deRichTool.get()->deRichDetectors();
  m_richS             = deRiches[ 0 ]->deRichSystem();

  // loop over riches and photo detector panels
  for ( unsigned int r = 0; r < deRiches.size(); ++r )
  {
    for ( unsigned int panel = 0; panel < Rich::NPDPanelsPerRICH; ++panel )
    {
      ( m_photoDetPanels[ deRiches[ r ]->rich() ] )[ panel ] =
        deRiches[ r ]->pdPanel( (Rich::Side)panel );
      _ri_debug << "Stored photodetector panel "
                << m_photoDetPanels[ deRiches[ r ]->rich() ][ panel ]->name()
                << " offset=" << m_photoDetPanels[ deRiches[ r ]->rich() ][ panel ]->localOffset()
                << endmsg;
    }
  }

  return sc;
}

//=============================================================================
// Finds the average positions of a vector of clusters, in global LHCb
// coordinates on the PD entrance window
//=============================================================================
LHCb::STL::Vector< Gaudi::XYZPoint >
SmartIDTool::globalPositions( const Rich::PDPixelCluster::Vector &clusters,
                              const bool                          ignoreClusters ) const
{
  // allocate the vector
  LHCb::STL::Vector< Gaudi::XYZPoint > points;
  points.reserve( clusters.size() );

  // Do we need to take the full cluster info into account
  if ( UNLIKELY( !ignoreClusters ) )
  {
    // scalar loop using full cluster info
    for ( const auto &clus : clusters )
    {
      points.emplace_back();
      const auto ok = globalPosition( clus, points.back() );
      if ( msgLevel( MSG::DEBUG ) )
      {
        if ( UNLIKELY( !ok ) )
        { _ri_debug << "Failed to compute global position for " << clus << endmsg; }
        else
        {
          _ri_debug << clus.primaryID() << " " << points.back() << endmsg;
        }
      }
    }
  }
  else
  {
    // Speed things up using only the primary IDs and SIMD methods...

    // scalar FP type for SIMD objects
    using FP = Rich::SIMD::DefaultScalarFP;
    // SIMD float type
    using SIMDFP = Rich::SIMD::FP< FP >;
    // SIMD Point
    using SIMDPoint = Rich::SIMD::Point< FP >;
    // Type for SmartIDs container.
    using SmartIDs = Rich::SIMD::STDArray< LHCb::RichSmartID >;

    // working SIMD objects
    SmartIDs  ids;
    SIMDPoint point;
    // SIMD index
    std::size_t index = 0;
    // Last PD pointer
    const DeRichPD *lastPD { nullptr };

    // Save functor
    auto saveInfo = [&]() {
      // Compute SIMD position
      lastPD->detectionPoint( ids, point, m_hitPhotoCathSide );
      // save scalar info to container
      for ( std::size_t i = 0; i < index; ++i )
      {
        points.emplace_back( point.X()[ i ], point.Y()[ i ], point.Z()[ i ] );
      } // reset SIMD index
      index  = 0;
      lastPD = nullptr;
    };

    // loop over the cluster info
    for ( const auto &clus : clusters )
    {
      // If new PD, or SIMD data is full, save and reset.
      if ( lastPD && ( lastPD != clus.dePD() || index >= SIMDFP::Size ) ) { saveInfo(); }
      // Fill info
      lastPD       = clus.dePD();
      ids[ index ] = clus.primaryID();
      // increment index for next time
      ++index;
    }

    // Save the last one
    if ( lastPD && index > 0 ) { saveInfo(); }
  }

  // debug sanity check
  assert( points.size() == clusters.size() );

  // return
  return points;
}

//=============================================================================
// Returns the position of a RichSmartID cluster in global coordinates
// on the PD entrance window
//=============================================================================
bool
SmartIDTool::globalPosition( const Rich::PDPixelCluster &cluster,
                             Gaudi::XYZPoint &           detectPoint ) const
{
  // Default return status is BAD
  bool sc = false;
  // cluster size
  const auto csize = cluster.size();
  if ( LIKELY( 1 == csize ) )
  {
    // Handle single pixel clusters differently for speed
    sc = _globalPosition( cluster.primaryID(), cluster.dePD(), detectPoint );
  }
  else if ( csize > 1 )
  {
    // start with primary ID position
    sc = _globalPosition( cluster.primaryID(), cluster.dePD(), detectPoint );
    // secondary IDs
    for ( const auto S : cluster.secondaryIDs() )
    {
      Gaudi::XYZPoint tmpP;
      sc = sc && _globalPosition( S, cluster.dePD(), tmpP );
      detectPoint += (Gaudi::XYZVector)tmpP;
    }
    // normalise
    detectPoint /= (double)csize;
  }
  return sc;
}

//=============================================================================
// Returns the position of a RichSmartID in global coordinates
// on the PD entrance window
//=============================================================================
bool
SmartIDTool::globalPosition( const LHCb::RichSmartID &smartID, Gaudi::XYZPoint &detectPoint ) const
{
  return _globalPosition( smartID, detectPoint );
}

//=============================================================================
// Returns the global position of a local coordinate, in the given RICH panel
//=============================================================================
Gaudi::XYZPoint
SmartIDTool::globalPosition( const Gaudi::XYZPoint &  localPoint,
                             const Rich::DetectorType rich,
                             const Rich::Side         side ) const
{
  return panel( rich, side )->PDPanelToGlobalMatrix() * localPoint;
}

//=============================================================================
// Returns the PD position (center of the silicon wafer)
//=============================================================================
bool
SmartIDTool::pdPosition( const LHCb::RichSmartID &pdid, Gaudi::XYZPoint &pdPoint ) const
{
  // Create temporary RichSmartIDs for two corners of the PD wafer
  LHCb::RichSmartID id1( pdid ), id0( pdid );
  id0.setPixelRow( pdid.idType() == LHCb::RichSmartID::MaPMTID ? 1 : 10 );
  id0.setPixelCol( pdid.idType() == LHCb::RichSmartID::MaPMTID ? 1 : 10 );
  id1.setPixelRow( pdid.idType() == LHCb::RichSmartID::MaPMTID ? 6 : 21 );
  id1.setPixelCol( pdid.idType() == LHCb::RichSmartID::MaPMTID ? 6 : 21 );

  // get position of each of these pixels
  Gaudi::XYZPoint a, b;
  const auto      sc = _globalPosition( id0, a ) && _globalPosition( id1, b );

  // return average position (i.e. PD centre)
  pdPoint = ( sc ? Gaudi::XYZPoint(
                     0.5 * ( a.x() + b.x() ), 0.5 * ( a.y() + b.y() ), 0.5 * ( a.z() + b.z() ) ) :
                   Gaudi::XYZPoint( 0, 0, 0 ) );
  return sc;
}

//=============================================================================
// Returns the SmartID for a given global position
//=============================================================================
bool
SmartIDTool::smartID( const Gaudi::XYZPoint &globalPoint, LHCb::RichSmartID &smartid ) const
{
  // check to see if the smartID is set, and if PD is active
  if ( smartid.pdIsSet() && !m_richS->pdIsActive( smartid ) ) { return false; }

  try
  {
    if ( globalPoint.z() < 8000.0 )
    {
      smartid.setRich( Rich::Rich1 );
      smartid.setPanel( globalPoint.y() > 0.0 ? Rich::top : Rich::bottom );
    }
    else
    {
      smartid.setRich( Rich::Rich2 );
      smartid.setPanel( globalPoint.x() > 0.0 ? Rich::left : Rich::right );
    }
    return panel( smartid )->smartID( globalPoint, smartid );
  }

  // Catch any GaudiExceptions thrown
  catch ( const GaudiException &excpt )
  {
    // Print exception as an error
    Error( "Caught GaudiException " + excpt.tag() + " message '" + excpt.message() + "'" );

    // reset smartid to a default one
    smartid = LHCb::RichSmartID();

    // return failure status
    return false;
  }
}

//=============================================================================
// Converts a point from the global frame to the detector panel frame
//=============================================================================
Gaudi::XYZPoint
SmartIDTool::globalToPDPanel( const Gaudi::XYZPoint &globalPoint ) const
{
  return (
    globalPoint.z() < 8000.0 ?
      // RICH1
      ( globalPoint.y() > 0.0 ?
          panel( Rich::Rich1, Rich::top )->globalToPDPanelMatrix() * globalPoint :
          panel( Rich::Rich1, Rich::bottom )->globalToPDPanelMatrix() * globalPoint ) : // RICH2
      ( globalPoint.x() > 0.0 ?
          panel( Rich::Rich2, Rich::left )->globalToPDPanelMatrix() * globalPoint :
          panel( Rich::Rich2, Rich::right )->globalToPDPanelMatrix() * globalPoint ) );
}

//=============================================================================
// Converts a SIMD position in global coordinates to the local coordinate system
//=============================================================================
SmartIDTool::SIMDPoint
SmartIDTool::globalToPDPanel( const Rich::DetectorType rich,
                              const Rich::Side         side,
                              const SIMDPoint &        globalPoint ) const
{
  return panel( rich, side )->globalToPDPanelMatrixSIMD() * globalPoint;
}

//=============================================================================
// Converts a SIMD position in global coordinates to the local coordinate system
//=============================================================================
SmartIDTool::SIMDPoint
SmartIDTool::globalToPDPanel( const Rich::DetectorType rich, const SIMDPoint &globalPoint ) const
{
  // mask for top/left
  const auto isTopLeft =
    ( Rich::Rich1 == rich ? globalPoint.y() > SIMDFP::Zero() : globalPoint.x() > SIMDFP::Zero() );
  if ( all_of( isTopLeft ) )
  {
    // all top/left
    return panel( rich, Rich::top )->globalToPDPanelMatrixSIMD() * globalPoint;
  }
  else if ( none_of( isTopLeft ) )
  {
    // all bottom/right
    return panel( rich, Rich::bottom )->globalToPDPanelMatrixSIMD() * globalPoint;
  }
  else
  {
    // need a mixture

    // top/left
    const auto pTL = panel( rich, Rich::top )->globalToPDPanelMatrixSIMD() * globalPoint;
    // bottom/right
    const auto pBR = panel( rich, Rich::bottom )->globalToPDPanelMatrixSIMD() * globalPoint;
    // get bottom/right X,Y,Z
    auto X = pBR.X();
    auto Y = pBR.Y();
    auto Z = pBR.Z();
    // set top/left as required
    X( isTopLeft ) = pTL.X();
    Y( isTopLeft ) = pTL.Y();
    Z( isTopLeft ) = pTL.Z();
    // return
    return { X, Y, Z };
  }
}

//=============================================================================
// Returns a list with all valid smartIDs
//=============================================================================
LHCb::RichSmartID::Vector
SmartIDTool::readoutChannelList() const
{
  // the list to return
  LHCb::RichSmartID::Vector readoutChannels;

  // Reserve rough size ( RICH1 + RICH2 )
  readoutChannels.reserve( 400000 );

  bool sc = true;

  // Fill for RICH1
  sc = sc && panel( Rich::Rich1, Rich::top )->readoutChannelList( readoutChannels );
  sc = sc && panel( Rich::Rich1, Rich::bottom )->readoutChannelList( readoutChannels );
  const auto nRich1 = readoutChannels.size();

  // Fill for RICH2
  sc = sc && panel( Rich::Rich2, Rich::left )->readoutChannelList( readoutChannels );
  sc = sc && panel( Rich::Rich2, Rich::right )->readoutChannelList( readoutChannels );
  const auto nRich2 = readoutChannels.size() - nRich1;

  if ( !sc ) Exception( "Problem reading readout channel lists from DeRichPDPanels" );

  // Sort the list
  SmartIDSorter::sortByRegion( readoutChannels );

  info() << "Created active PD channel list : # channels RICH(1/2) = " << nRich1 << " / " << nRich2
         << endmsg;

  if ( msgLevel( MSG::VERBOSE ) )
  {
    for ( const auto &ID : readoutChannels )
    {
      Gaudi::XYZPoint gPos;
      sc = _globalPosition( ID, gPos );
      if ( !sc ) Exception( "Problem converting RichSmartID to global coordinate" );
      verbose() << " RichSmartID " << ID << " " << ID.dataBitsOnly().key() << endmsg
                << "     -> global Position : " << gPos << endmsg
                << "     -> local Position  : " << globalToPDPanel( gPos ) << endmsg;
    }
  }

  return readoutChannels;
}

//=============================================================================

DECLARE_COMPONENT( SmartIDTool )

//=============================================================================
