
//-----------------------------------------------------------------------------
/** @file RichRadiatorTool.cpp
 *
 *  Implementation file for tool : Rich::Future::RadiatorTool
 */
//-----------------------------------------------------------------------------

// Array properties
#include "GaudiKernel/ParsersFactory.h"
#include "GaudiKernel/StdArrayAsProperty.h"

// local
#include "RichRadiatorTool.h"

// Rich Det
#include "RichDet/DeRich.h"

using namespace Rich::Future;

//=============================================================================

RadiatorTool::RadiatorTool( const std::string &type,
                            const std::string &name,
                            const IInterface * parent )
  : ToolBase( type, name, parent )
{
  // interface
  declareInterface< IRadiatorTool >( this );
  // Debug messages
  // setProperty( "OutputLevel", 1 );
}

//=============================================================================

StatusCode
RadiatorTool::initialize()
{
  const auto sc = ToolBase::initialize();
  if ( !sc ) return sc;

  // load the required radiators
  for ( const auto rad : { Rich::Aerogel, Rich::Rich1Gas, Rich::Rich2Gas } )
  {
    if ( m_usedRads[ rad ] ) { loadRadiator( rad ); }
  }

  // return
  return sc;
}

//=============================================================================

StatusCode
RadiatorTool::finalize()
{
  updMgrSvc()->unregister( this );
  return ToolBase::finalize();
}

//=============================================================================

void
RadiatorTool::loadRadiator( const Rich::RadiatorType radiator )
{
  StatusCode sc = StatusCode::SUCCESS;
  if ( radiator == Rich::Rich1Gas )
  {
    sc = updateRich1Gas();
    updMgrSvc()->registerCondition(
      this, DeRichLocations::Rich1Gas, &RadiatorTool::updateRich1Gas );
  }
  else if ( radiator == Rich::Rich2Gas )
  {
    sc = updateRich2Gas();
    updMgrSvc()->registerCondition(
      this, DeRichLocations::Rich2Gas, &RadiatorTool::updateRich2Gas );
  }
  else if ( radiator == Rich::Aerogel )
  {
    sc = updateAerogel();
    updMgrSvc()->registerCondition( this, DeRichLocations::Aerogel, &RadiatorTool::updateAerogel );
  }
  if ( sc.isFailure() )
  {
    std::ostringstream mess;
    mess << "Problem loading radiator " << radiator;
    Exception( mess.str() );
  }
}

//=============================================================================
// UMS update
//=============================================================================

StatusCode
RadiatorTool::updateRich1Gas()
{
  m_radiators[ Rich::Rich1Gas ].clear();
  m_radiators[ Rich::Rich1Gas ].push_back( getDet< DeRichRadiator >( DeRichLocations::Rich1Gas ) );
  return StatusCode::SUCCESS;
}

//=============================================================================

StatusCode
RadiatorTool::updateRich2Gas()
{
  m_radiators[ Rich::Rich2Gas ].clear();
  m_radiators[ Rich::Rich2Gas ].push_back( getDet< DeRichRadiator >( DeRichLocations::Rich2Gas ) );
  return StatusCode::SUCCESS;
}

//=============================================================================

StatusCode
RadiatorTool::updateAerogel()
{
  m_radiators[ Rich::Aerogel ].clear();
  const auto *aerogel = getDet< DeRichMultiSolidRadiator >( DeRichLocations::Aerogel );
  m_radiators[ Rich::Aerogel ].reserve( aerogel->radiators().size() );
  for ( const auto *dRad : aerogel->radiators() )
  {
    const auto *d = dynamic_cast< const DeRichAerogelRadiator * >( dRad );
    if ( !d ) return Error( "Failed to cast to DeRichAerogelRadiator" );
    if ( std::find( m_excludedAeroTiles.begin(), m_excludedAeroTiles.end(), d->primaryTileID() ) ==
         m_excludedAeroTiles.end() )
    { m_radiators[ Rich::Aerogel ].push_back( dRad ); }
    else
    {
      std::ostringstream mess;
      mess << "Ignoring Aerogel tile " << d->primaryTileID();
      Warning( mess.str(), StatusCode::SUCCESS ).ignore();
    }
  }

  // Sort by distance from beam line
  std::stable_sort( m_radiators[ Rich::Aerogel ].begin(),
                    m_radiators[ Rich::Aerogel ].end(),
                    SortByDistFromBeam() );

  return StatusCode::SUCCESS;
}

//=============================================================================

unsigned int
RadiatorTool::intersections( const Gaudi::XYZPoint &        globalPoint,
                             const Gaudi::XYZVector &       globalVector,
                             const Rich::RadiatorType       radiator,
                             Rich::RadIntersection::Vector &intersections ) const
{

  // tally for the number of intersections
  unsigned int totalIntersections( 0 );

  // loop over all volumes for given radiator
  /** @todo With aerogel sub-tiles, there are now a lot of volumes
   *        need to investigate a faster way to do this search than
   *        just looping over them all */
  for ( const auto *R : m_radiators[ radiator ] )
  {
    // entry and exit points
    Gaudi::XYZPoint entry, exit;
    // do we intersect ?
    if ( R->intersectionPoints( globalPoint, globalVector, entry, exit ) )
    {
      // save this intersection
      intersections.emplace_back( entry, globalVector, exit, globalVector, R );
      ++totalIntersections;
    }
  }

  // If more than one intersection sort into increasing z position
  if ( totalIntersections > 1 ) { Rich::RadIntersection::Sorter::sortByZ( intersections ); }

  // debug printout
  if ( msgLevel( MSG::DEBUG ) )
  {
    debug() << radiator << " Intersections :";
    for ( const auto &S : intersections )
    { debug() << " " << S.entryPoint() << "->" << S.exitPoint(); }
    debug() << endmsg;
  }

  // return the number of intersections
  return totalIntersections;
}

//=============================================================================

DECLARE_COMPONENT( RadiatorTool )

//=============================================================================
