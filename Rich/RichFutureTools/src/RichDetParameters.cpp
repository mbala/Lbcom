
//-----------------------------------------------------------------------------
/** @file RichDetParameters.cpp
 *
 * Implementation file for class : Rich::DetParameters
 *
 * @author Chris Jones   Christopher.Rob.Jones@cern.ch
 * @date 14/01/2002
 */
//-----------------------------------------------------------------------------

// local
#include "RichDetParameters.h"

using namespace Rich::Future;

DECLARE_COMPONENT( DetParameters )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
DetParameters::DetParameters( const std::string &type,
                              const std::string &name,
                              const IInterface * parent )
  : ToolBase( type, name, parent )
{
  // Init outer radiator limits for a single PD panel (unsigned)
  m_radOutLimLoc = {
    RadLimits( 0, 625, 0, 600 ),   // Aerogel
    RadLimits( 0, 625, 180, 600 ), // R1Gas
    RadLimits( 50, 690, 0, 696 )   // R2Gas
  };
  // interface
  declareInterface< IDetParameters >( this );
}

StatusCode
DetParameters::initialize()
{
  // Initialise base class
  auto sc = ToolBase::initialize();
  if ( !sc ) return sc;

  // Initialise the energy info.
  return setPhotonParameters();
}

StatusCode
DetParameters::setPhotonParameters()
{
  StatusCode sc = StatusCode::SUCCESS;

  // load RICH1
  const auto rich1DE = getDet< DeRich >( DeRichLocations::Rich1 );
  if ( rich1DE )
  {

    // max photon energies
    m_maxPhotEn = {
      ( rich1DE->param< double >( "PhotonCkvMaximumEnergyAerogel" ) ) / Gaudi::Units::eV,
      ( rich1DE->param< double >( "PhotonCkvMaximumEnergyC4F10" ) ) / Gaudi::Units::eV,
      ( rich1DE->param< double >( "PhotonCkvMaximumEnergyCF4" ) ) / Gaudi::Units::eV
    };

    // min energies
    m_minPhotEn = {
      ( rich1DE->param< double >( "PhotonCkvMinimumEnergyAerogel" ) ) / Gaudi::Units::eV,
      ( rich1DE->param< double >( "PhotonCkvMinimumEnergyC4F10" ) ) / Gaudi::Units::eV,
      ( rich1DE->param< double >( "PhotonCkvMinimumEnergyCF4" ) ) / Gaudi::Units::eV
    };

    // For now the average kept such that it is the mean of the max and min.
    // This may need to be reviewed in the future.
    m_meanPhotEn = { 0.5 * ( m_maxPhotEn[ 0 ] + m_minPhotEn[ 0 ] ),
                     0.5 * ( m_maxPhotEn[ 1 ] + m_minPhotEn[ 1 ] ),
                     0.5 * ( m_maxPhotEn[ 2 ] + m_minPhotEn[ 2 ] ) };

    // standard deviations
    // if PMTs change default values
    // Do we have HPDs or PMTs ?
    const bool isPmt = ( rich1DE->RichPhotoDetConfig() == Rich::PMTConfig );
    if ( isPmt ) { m_refSD = { 0.488e-3, 0.2916e-4, 0.6777e-5 }; }
    // If the values exist in the DB, use these
    if ( rich1DE->exists( "RichBrunelRefIndexSDParameters" ) )
    {
      const auto aRefSD = rich1DE->paramVect< double >( "RichBrunelRefIndexSDParameters" );
      m_refSD           = { aRefSD[ 0 ], aRefSD[ 1 ], aRefSD[ 2 ] };
    }
  }
  else
  {
    sc = Warning( "Failed to load DeRich1" );
  }

  return sc;
}

double
DetParameters::maxPhotonEnergy( const Rich::RadiatorType rad ) const
{
  return m_maxPhotEn[ rad ];
}

double
DetParameters::minPhotonEnergy( const Rich::RadiatorType rad ) const
{
  return m_minPhotEn[ rad ];
}

double
DetParameters::meanPhotonEnergy( const Rich::RadiatorType rad ) const
{
  return m_meanPhotEn[ rad ];
}

const Rich::IDetParameters::RadLimits &
DetParameters::AvAcceptOuterLimitsLocal( const Rich::RadiatorType rad ) const
{
  return m_radOutLimLoc[ rad ];
}

double
DetParameters::refIndexSD( const Rich::RadiatorType rad ) const
{
  return m_refSD[ rad ];
}
