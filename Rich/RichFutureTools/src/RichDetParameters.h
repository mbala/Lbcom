
//-----------------------------------------------------------------------------
/** @file RichDetParameters.h
 *
 *  Header file for tool : Rich::DetParameters
 *
 *  @author Chris Jones    Christopher.Rob.Jones@cern.ch
 *  @date   2004-03-29
 */
//-----------------------------------------------------------------------------

#pragma once

// Gaudi ( must be first ...)
#include "GaudiKernel/ParsersFactory.h"
#include "GaudiKernel/StdArrayAsProperty.h"
#include "GaudiKernel/SystemOfUnits.h"

// Base class and interface
#include "RichFutureKernel/RichToolBase.h"
#include "RichInterfaces/IRichDetParameters.h"

// RichDet
#include "RichDet/DeRich.h"

namespace Rich::Future
{

  //-----------------------------------------------------------------------------
  /** @class DetParameters RichDetParameters.h
   *
   *  Tool to provide access to useful detector parameters, that cannot be easily
   *  calculated on the fly. Some could eventually be moved to either the LHCbCond
   *  or DDDB databases.
   *
   *  @author Chris Jones         Christopher.Rob.Jones@cern.ch
   *  @date   2004-03-29
   *
   *  @todo Replace hardcoded data with access to XML/D-Base
   */
  //-----------------------------------------------------------------------------

  class DetParameters final : public ToolBase, virtual public IDetParameters
  {

  public: // Methods for Gaudi Framework

    /// Standard constructor
    DetParameters( const std::string &type, const std::string &name, const IInterface *parent );

    // Initialization of the tool after creation
    StatusCode initialize() override;

  public: // methods (and doxygen comments) inherited from interface

    // Calculates the maximum observable photon energy for a given radiator medium
    double maxPhotonEnergy( const Rich::RadiatorType rad ) const override;

    // Calculates the minimum observable photon energy for a given radiator medium
    double minPhotonEnergy( const Rich::RadiatorType rad ) const override;

    // Calculates the mean observable photon energy for a given radiator medium
    double meanPhotonEnergy( const Rich::RadiatorType rad ) const override;

    // Calculate the standard deviation of n-1 distribution for observed photons
    double refIndexSD( const Rich::RadiatorType rad ) const override;

    // Returns the average acceptance outer limits in local HPD coordinates
    // for the given radiator type
    const IDetParameters::RadLimits &
    AvAcceptOuterLimitsLocal( const Rich::RadiatorType rad ) const override;

  private:

    /// Read the DB to set photon energy parameters
    StatusCode setPhotonParameters();

  private:

    /// The maximum photon energies
    Gaudi::Property< RadiatorArray< double > > m_maxPhotEn { this,
                                                             "MaxPhotonEnergy",
                                                             { 4.0, 7.0, 7.0 } };

    /// The minimum photon energies
    Gaudi::Property< RadiatorArray< double > > m_minPhotEn { this,
                                                             "MinPhotonEnergy",
                                                             { 1.75, 1.75, 1.75 } };

    /// The mean photon energies
    Gaudi::Property< RadiatorArray< double > > m_meanPhotEn { this,
                                                              "MeanPhotonEnergy",
                                                              { 2.89, 4.29, 4.34 } };

    /// Standard deviation of n-1 distribution for observed photons
    Gaudi::Property< RadiatorArray< double > > m_refSD { this,
                                                         "RefIndexSD",
                                                         { 0.488e-3, 0.393e-4, 0.123e-4 } };

    /// The radiator acceptance limits
    RadiatorArray< IDetParameters::RadLimits > m_radOutLimLoc = { {} };
  };

} // namespace Rich::Future
