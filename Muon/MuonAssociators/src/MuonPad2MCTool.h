#ifndef MUONPAD2MCTOOL_H 
#define MUONPAD2MCTOOL_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "MCInterfaces/IMuonPad2MCTool.h"            // Interface

#include "MuonDet/DeMuonDetector.h"
#include "MuonDet/MuonDAQHelper.h"

/** @class MuonPad2MCTool MuonPad2MCTool.h
 *  
 *
 *  @author Alessia Satta
 *  @date   2006-12-06
 */
class MuonPad2MCTool : public extends<GaudiTool, IMuonPad2MCTool> {
public: 
  /// Standard constructor
  MuonPad2MCTool( const std::string& type, 
                  const std::string& name,
                  const IInterface* parent);

  StatusCode initialize() override;
  LHCb::MCParticle* Pad2MC(LHCb::MuonTileID value) const override;
  
  bool isXTalk(LHCb::MuonTileID value,LHCb::MCParticle*& pp) const override;
  LHCb::MCParticle* PadNoXtalk2MC(LHCb::MuonTileID value) const override;

private:
  StatusCode XtalkStrip(LHCb::MuonTileID tile,LHCb::MCParticle*& pp) const;
  StatusCode XtalkPad(LHCb::MuonTileID tile,LHCb::MCParticle*& pp) const;
  
  DeMuonDetector*   m_muonDetector;

};
#endif // MUONPAD2MCTOOL_H
