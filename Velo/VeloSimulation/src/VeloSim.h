#pragma once

// Include files
// from STL
#include <string>

// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"
#include "GaudiKernel/RndmGenerators.h"
#include "GaudiKernel/Point3DTypes.h"

#include <cmath>
// and FPE gaurd to protect this
#include "Kernel/FPEGuard.h"

// from Velo
#include "Event/MCVeloFE.h"

class DeVelo;
class MCHit;
class VeloChannelID;
class ISiAmpliferResponse;
struct ISiDepositedCharge;
class IRadDamageTool;

/** @class VeloSim VeloSim.h VeloAlgs/MCVeloSim.h
 *
 * Fill MCVeloDigit, based on MCHit
 * In general would contain silicon and Front End Chip Simulation
 * and digitisation.
 * Currently just a fast test version.
 *  @author Chris Parkes, based on code of Oliver Callot; update Tomasz Szumlak
 *  @date   10/01/02
 */

class VeloSim final : public GaudiAlgorithm {
public:
  //
  /// Standard constructor
  VeloSim( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override;    ///< Algorithm initialization
  StatusCode execute   () override;    ///< Algorithm execution
  StatusCode finalize  () override;    ///< Algorithm finalise

private:
  static const FPE::Guard::mask_type Inexact;
  static const FPE::Guard::mask_type AllExcept;

  ///protected gsl_erf_sf_q against FPE: integral of normal dist from arg->inf.
  inline double erf_Q(double const &arg) const {
    // remove underflows by ignoring results different from 0 or 1 by < 1e-24
    if(fabs(arg)>10.) return (arg>0. ? 0. : 1.);
    // turn off the inexact FPE (always goes off for this function)
    static const auto c_sqrt1_2 = 1.0/std::sqrt(2);
    auto reducedFPE = FPE::Guard{Inexact, true};
    return 0.5*std::erfc( c_sqrt1_2*arg );
  }

  ///protected poisson distribution against floating point exceptions
  inline int safe_int_poissonDist() {
    // turn off FPEs
    auto allFPE = FPE::Guard{AllExcept, true};
    return int(m_poissonDist());
  }

  /// process requested simulation steps
  void simulation(LHCb::MCHits * veloHits, double timeOffset);

  /// Simulation of charge deposition between entry and exit of MCHit
  void chargeSim(LHCb::MCHits * hits, double timeOffset);

  /// Number of points to deposite charge along the trajectory of the hit
  int simPoints(LHCb::MCHit* hit);

  /// spread the charge over the points to simulate
  void chargePerPoint(LHCb::MCHit* hit,
                      std::vector<double>& Spoints, double timeOffset);

  /// add delta ray charges if required
  void deltaRayCharge(double charge, double tol, std::vector<double>& Spoints);

  /// allow the deposited charge points to diffuse to the strips
  void diffusion(LHCb::MCHit* hit,
                 std::vector<double>& Spoints);

  /// add the charge to a front end channel with MChit link (if not 0)
  void fillFE(LHCb::MCVeloFE* myFE, LHCb::MCHit* hit, double charge);

  /// add the charge to a front end channel, no MC link
  inline void fillFE(LHCb::MCVeloFE* myFE, double charge){
    fillFE(myFE, 0, charge);
  }

  /// setup capacitive coupling
  void coupling();

  /// From an originally sorted list, find the strip with the previous key,
  /// or create a new one.
  LHCb::MCVeloFE* findOrInsertPrevStrip(LHCb::MCVeloFEs::iterator FEIt,
                                        bool& valid, bool create);

  /// From an originally sorted list, find the strip with the next key,
  /// or create a new one.
  LHCb::MCVeloFE* findOrInsertNextStrip(LHCb::MCVeloFEs::iterator FEIt,
                                        bool& valid, bool create);

  void pedestalSim(); ///< add a pedestal value to the FEs
  void noiseSim();    ///< Add noise to the strips
  /// Added noise of a strip from strip capacitance
  double noiseValue(double noiseSigma);
  /// Noise to add to an otherwise empty strip from strip capacitance
  double noiseValueTail(double noiseSigma, double threshold);
  void CMSim(); ///< Common mode simulation
  void deadStrips(); ///< Simulate random dead strips
  void finalProcess(); ///<remove any MCFEs with charge below abs(threshold)
  StatusCode storeOutputData(); ///< put the data on the TES
  LHCb::MCVeloFE* findOrInsertFE(LHCb::VeloChannelID& stripKey);

  /// gets the effective charge fraction for this hit
  /// compared to an in time main bunch hit
  double chargeTimeFactor(double TOF, double bunchOffset, double z);

  /// check conditions Data Base
  bool checkConditions(const LHCb::MCHit& aHit);

  /// fraction of charge lost to 2nd metal layer on on R, and which strip
  double metal2Loss(const DeVeloSensor* sens, const Gaudi::XYZPoint& point,
                    LHCb::VeloChannelID &chM2);

  // data members

  // Input/Ouput
  Gaudi::Property<std::vector<std::string>>
    m_inputContainers{this, "InputContainers",
      std::vector<std::string>{}, "Name of input containers"};

  Gaudi::Property<std::vector<double>>
    m_inputTimeOffsets{this, "InputTimeOffsets",
      std::vector<double>{}, "delta T0 of the input containers"};

  Gaudi::Property<std::vector<std::string>>
    m_outputContainers{this, "OutputContainers",
      std::vector<std::string>{}, "Name of output containers"};

  Gaudi::Property<std::string>
    m_MCHitContainerToLinkName{this,"InputContainerToLink","",
      "Name of MCHit container to make links to"};

  // Which features to simulate
  Gaudi::Property<bool>
    m_coupling{this,"Coupling", true, "Simulate capactive coupling"};

  Gaudi::Property<bool>
    m_noiseSim{this,"NoiseSim",true, "Do noise simulation"};

  Gaudi::Property<bool>
    m_simNoisePileUp{this,"SimNoisePileUp",false,
      "velo or pileup for noise simulation"};

  Gaudi::Property<bool>
    m_pedestalSim{this,"PedestalSim",true,"Simulate pedestal variations"};

  Gaudi::Property<bool>
    m_CMSim{this,"CMSim",true,"Simulate common mode effects"};

  Gaudi::Property<bool>
    m_inhomogeneousCharge{this,"InhomogeneousCharge",true,
      "simulate unequal charge along track"};

  // Parameters for simulation
  Gaudi::Property<double>
    m_stripInefficiency{this,"StripInefficiency", 0.0,
      "Fraction of strips to randomly kill"};

  // Thresholds : define in ADCs, convert to e- count here
  // In VeloTell1DataProcessor  ADC in e- = (ADC_max)/(e_max)
  // 76950/128 = 601
  // tuned to 2009 data
  Gaudi::Property<double>
    m_electronsPerADC{this, "ElectronsPerADC", 601.,
      "convert noise sources in ADC to electrons"};

  Gaudi::Property<double> m_thresholdADC{this,"ThresholdADC",4.0,
      "threshold charge to make VeloFE from strip in ADC counts next to a signal"};
  /// threshold charge in electrons = m_thresholdADC*m_electronsPerADC
  double m_threshold;

  Gaudi::Property<double> m_thresholdADCSingle{this, "ThresholdADCSingle", 10.0,
      "threshold charge to make an isolated VeloFE from strip in ADC counts"};
  /// threshold charge in electrons = m_thresholdADC*m_electronsPerADC
  double m_thresholdSingle;

  Gaudi::Property<double> m_kT{this,"kT",0.025,
      "sqrt(2*m_kT/m_biasVoltage) is the expected diffusion width"};

  Gaudi::Property<double> m_biasVoltage{this, "BiasVoltage", 150.,
      "voltage applied to (over)depleted the sensor"};

  Gaudi::Property<double> m_eVPerElectron{this,"eVPerElectron",3.6,
      "Amount of energy required to make an e/hole pair in silicon"};

  Gaudi::Property<int>
    m_simulationPointsPerStrip{this, "SimulationPointsPerStrip", 3 ,
      "Number of points to distribute charge on per strip"};

  Gaudi::Property<double> m_chargeUniform{this, "ChargeUniform", 70.,
      "Use same charge along strip at each seeding centre"};

  Gaudi::Property<double> m_deltaRayMinEnergy{this,"DeltaRayMinEnergy", 1000.,
      "Minimum energy for delta ray simulation"};

  Gaudi::Property<double> m_capacitiveCoupling{this, "CapacitiveCoupling", 0.05,
      "fraction next strip charge sharing due to capacitive coupling to simulate"};

  Gaudi::Property<double> m_averageStripNoise{this, "AverageStripNoise", 1.85,
      "Used to estimate how many strips to simulate noise in"};

  Gaudi::Property<double> m_offPeakSamplingTime{this, "OffPeakSamplingTime", 0.,
      "Time offset of sample to peak time"};

  Gaudi::Property<bool>
    m_makeNonZeroSuppressedData{this, "MakeNonZeroSuppressedData", false,
      "make a VeloFE for every strip (default false to make ZS data)"};

  Gaudi::Property<double> m_pedestalConst{this, "PedestalConst", 512.,
      "Simulated pedestal"};

  Gaudi::Property<double> m_pedestalVariation{this,"PedestalVariation",0.05,
      "Simulated pedestal variation"};

  Gaudi::Property<std::string>
    m_depChargeToolType{this, "DepChargeTool", "SiDepositedCharge",
      "Name of tool to calculate charge"};

  Gaudi::Property<bool> m_useDepTool{this,"UseDepTool",true,
      "Allows to use GEANT energy directly if false"};

  Gaudi::Property<std::string>
    m_SiTimeToolType{this,"SiAmplifierResponse", "SiAmplifierResponse",
      "normally 'SiAmpliferResponse' from STTools"};

  Gaudi::Property<double>
    m_pulseShapePeakTime{this,"PulseShapePeakTime",30.7848,
      "Time of the peak amplifier response"};

  Gaudi::Property<double> m_noiseScale{this,"NoiseScale",1.,
      "Scale factor to apply to noise from db"};

  Gaudi::Property<double> m_scaleFluctuations{this,"FluctuationsScale",1.,
      "Scale factor to apply to Gaussian flucuations in charge at points"};

  Gaudi::Property<std::string>
    m_radToolType{this,"RadDamageToolName","VeloRadDamageTool",
      "Name of the IRadDamageTool to use"};

  Gaudi::Property<bool> m_intraSensorCrossTalk{this,"IntraSensorCrossTalk",true,
      "Simulate intrasensor crosstalk to 2nd metal routing lines"};

  Gaudi::Property<bool> m_m2SinglePoint{this,"M2SinglePoint",true,
      "simulate the metal2 coupling for the mid point or all points"};

  /// MCHit container to make links to
  ObjectContainerBase* m_MCHitContainerToLink = nullptr;
  //
  DeVelo* m_veloDet = nullptr; ///< Detector Element
  LHCb::MCVeloFEs m_FEs; ///< vector of FE output  signals
  LHCb::MCVeloFEs m_FEs_coupling;

  // allows resolution to be degraded for robustness studies
  Gaudi::XYZPoint m_movePosition; // used when degrading resolution
  Rndm::Numbers m_gaussDist;  ///< gaussian random numbers
  Rndm::Numbers m_uniformDist;///< uniform random numbers
  ///< poisson random numbers, mean = av. number of noise strips in a sensor
  Rndm::Numbers m_poissonDist;

  double ran_inv_E2(double Emin, double Emax); ///<random numbers from 1/E^2
  /// Random numbers from a gaussian tail
  double ran_gaussian_tail(const double a, const double sigma);
  /// probability to add noise tail to otherwise empty strip next to signal
  double m_noiseTailProb = 0;
  /// probability to add noise tail to otherwise empty strip, isolated
  double m_noiseTailProbSingle = 0;
  double m_baseDiffuseSigma = 0;    ///< base size of charge diffusion

  ISiAmplifierResponse* m_SiTimeTool = nullptr;
  ISiDepositedCharge* m_depositedCharge = nullptr;///< Tool calculates accumulated charge

  unsigned int m_totalFEs = 0; ///< Number of FEs created
  unsigned int m_killedFEsRandom = 0; ///< Number of FEs removed by random kills
  unsigned int m_killedFEsBadStrips = 0; ///< Number of FEs removed by bad strips

  /// the type of tool to use for rad damage
  IRadDamageTool* m_radTool = nullptr;


};
