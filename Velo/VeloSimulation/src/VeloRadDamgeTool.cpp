// Include files

// from MCEvent
#include "Event/MCHit.h"

// VeloDet
#include "VeloDet/DeVelo.h"

// local
#include "VeloRadDamgeTool.h"

//-----------------------------------------------------------------------------
// Implementation file for class : VeloRadDamageTool
//
// 2011-03-31 : David Hutchcroft
//-----------------------------------------------------------------------------

DECLARE_COMPONENT( VeloRadDamageTool )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
VeloRadDamageTool::VeloRadDamageTool(const std::string& type,
                                     const std::string& name,
                                     const IInterface* parent)
  : GaudiTool ( type, name , parent )
{
  declareInterface<IRadDamageTool>(this);
}


StatusCode VeloRadDamageTool::initialize(){
  StatusCode sc = GaudiTool::initialize();
  if (sc.isFailure()) return Error("Failed to initialize", sc);

  m_veloDet = getDet<DeVelo>( DeVeloLocation::Default );

  if( !existDet<DataObject>(detSvc(),"Conditions/HardwareProperties/Velo/RadiationDamage") ){
    Warning("VELO radiation damage parameters not in conditions DB",
            StatusCode::SUCCESS).ignore();

    for( auto iSens = m_veloDet->sensorsBegin(); iSens != m_veloDet->sensorsEnd(); ++iSens ){
      RadParam param;
      param.useConst = true;
      // fully set param so copy and finish this sensor
      m_radParamMap[(*iSens)->sensorNumber()] = std::move(param);

      // do the same for the diffusion scaling
      DiffusionParam dParam; 
      dParam.useConst = true;
      dParam.constDiffusion = 1.;
      m_diffParamMap[(*iSens)->sensorNumber()] = std::move(dParam);
    }
    return StatusCode::SUCCESS;
  }

  for(auto iSens = m_veloDet->sensorsBegin(); iSens != m_veloDet->sensorsEnd() ; ++iSens ){

    std::string conditionName =
      format("Conditions/HardwareProperties/Velo/RadiationDamage/Sensor%03i-RadiationParam",(*iSens)->sensorNumber());

    if( !existDet<Condition>(detSvc(),conditionName) ){
      return Warning("Condition directory exists but missing condition " +
                     conditionName,StatusCode::FAILURE);
    }
    m_radConditionMap[(*iSens)->sensorNumber()] = get<Condition>(detSvc(),conditionName);
    registerCondition(conditionName,
                      &VeloRadDamageTool::i_loadCondtion);
    if(msgLevel( MSG::VERBOSE ))
      verbose() << "Registered " << conditionName
                << " into m_radConditionMap[" << (*iSens)->sensorNumber()
                << "] = " << m_radConditionMap[(*iSens)->sensorNumber()]
                << endmsg;
  }
  sc = i_loadCondtion();
  sc = runUpdate();
  return sc;
}

StatusCode VeloRadDamageTool::i_loadCondtion(){

  StatusCode sc(StatusCode::SUCCESS);
  for(auto iSens = m_veloDet->sensorsBegin(); iSens != m_veloDet->sensorsEnd(); ++iSens ){

    unsigned int sNum = (*iSens)->sensorNumber();

    if(msgLevel( MSG::DEBUG )) debug() << "For sensor " << sNum << endmsg;
    // param is the current parameter (created if not existing already)
    RadParam &param = m_radParamMap[sNum];

    sc = loadDiffusion(**iSens);
    if (!sc) return sc;

    param.useConst = m_radConditionMap[sNum]->param<int>("UseConst");
    if( param.useConst ) {
      param.constantFrac =
        m_radConditionMap[sNum]->param<double>("ConstFrac");
      // no spline for const output
      if(msgLevel( MSG::DEBUG )) debug() << "Constant charge fraction " << param.constantFrac
                                         << endmsg;
    }else{
      // so need spline
      std::vector<double> radii =
        m_radConditionMap[sNum]->param<std::vector<double> >("Radii");
      std::vector<double> chargeFrac =
        m_radConditionMap[sNum]->param<std::vector<double> >("ChargeFrac");

      if(radii.size() == 0 ||
         ( radii.size() != chargeFrac.size() ) )
        return Error(format("radii and chargeFrac not consistent for sensor %i",sNum),
                     StatusCode::FAILURE);

      // outside range make a constant at the extremal value
      param.rMin = radii.front();
      param.rMax = radii.back();
      param.responseMin = chargeFrac.front();
      param.responseMax = chargeFrac.back();

      param.responseSpline.reset(
        new GaudiMath::SimpleSpline( radii, chargeFrac,
                                     GaudiMath::Interpolation::Linear ) );

      if(msgLevel( MSG::DEBUG )){
        debug() << "Spline parameters: pointer" << param.responseSpline.get() << endmsg;
        debug() << "radius      ChargeFraction" <<endmsg;
        for ( unsigned int i = 0 ; i < radii.size() ; ++i ){
          debug() << format("%5.2f   %5.2f",
                            radii[i],chargeFrac[i])
                  << endmsg;
        }
      }
      if(msgLevel( MSG::VERBOSE )) dumpResponse(**iSens);
    }

    if( !(*iSens)->isR()  ){
      param.chargeFracWidth = 1.;
      param.chargeFracMax = 0.;
      param.stripDistScale = 1.;
      param.fracOuter = 0.;
    }else{
      // stuff for the M2 coupling (applies only to R sensors)
      try{
        param.chargeFracWidth = m_radConditionMap[sNum]->param<double>("ChargeFracWidth");
        param.chargeFracMax = m_radConditionMap[sNum]->param<double>("ChargeFracMax");
        param.stripDistScale = m_radConditionMap[sNum]->param<double>("StripDistScale");
        param.fracOuter = m_radConditionMap[sNum]->param<double>("FractionFromOuter");
      }catch( ParamException &e ){
        // in case code runs on old conditions
        param.chargeFracWidth = 1.;
        param.chargeFracMax = 0.;
        param.stripDistScale = 1.;
        param.fracOuter = 0.;
        Warning("Missing M2 coupling conditions using default values",StatusCode::SUCCESS,1).ignore();
      }
      std::vector<double> trackEta,twoMetalFraction;
      try{
	trackEta =
	  m_radConditionMap[sNum]->param<std::vector<double> >("trackEta");
	twoMetalFraction =
	  m_radConditionMap[sNum]->param<std::vector<double> >("twoMetalFraction");

      }catch( ParamException &e ){
	if(msgLevel( MSG::DEBUG )){
	  debug() << "Undefined trackEta and twoMetalFraction, use null values" << endmsg;
	}
	trackEta.resize(1,3.5); // single value middle of the range
	twoMetalFraction.resize(1,1.);

      }

      if(trackEta.size() == 0 ||
         ( trackEta.size() != twoMetalFraction.size() ) )
        return Error(format("trackEta and twoMetalFraction not consistent for sensor %i",sNum),
                     StatusCode::FAILURE);

      // outside range make a constant at the extremal value
      param.etaMin = trackEta.front();
      param.etaMax = trackEta.back();
      param.twoMetalFractionMin = twoMetalFraction.front();
      param.twoMetalFractionMax = twoMetalFraction.back();

      param.couplingSpline.reset(new GaudiMath::SimpleSpline( trackEta,twoMetalFraction,
							      GaudiMath::Interpolation::Linear ) );

      if(msgLevel( MSG::DEBUG )){
        debug() << "Spline parameters: pointer" << param.couplingSpline.get() << endmsg;
        debug() << "trackEta     twoMetalFraction" <<endmsg;
        for ( unsigned int i = 0 ; i < trackEta.size() ; ++i ){
          debug() << format("%5.2f   %5.2f",
                            trackEta[i],twoMetalFraction[i])
                  << endmsg;
        }
	debug() << "limits " << param.etaMin << " to " <<  param.etaMax
		<< " with factors " << param.twoMetalFractionMin << " to " 
		<< param.twoMetalFractionMax << endmsg;
	if(msgLevel( MSG::VERBOSE )) dumpM2Response(**iSens);
      }
    }

    if (m_radConditionMap[sNum]->exists("DiffusionScale")){
      param.diffusionScale =  m_radConditionMap[sNum]->param<double>("DiffusionScale");
      if(msgLevel( MSG::DEBUG )){
	debug() << "DiffusionScale "<<param.diffusionScale  << endmsg;
      }
    }else{
      if(msgLevel( MSG::DEBUG )){
	debug() << "Missing parameter DiffusionScale, default = 1" << endmsg;
      }
    }

    if (m_radConditionMap[sNum]->exists("PitchScaling")) {
      param.pitchScaling =
          m_radConditionMap[sNum]->param<double>("PitchScaling");
      if (msgLevel(MSG::DEBUG)) {
        debug() << "PitchScaling " << param.pitchScaling << endmsg;
      }
    } else {
      if (msgLevel(MSG::DEBUG) && (*iSens)->isR()) {
        debug() << "Missing parameter PitchScaling, default = 0." << endmsg;
      }
    }

    // if debug/verbose dump parameters to the screen
    if (msgLevel(MSG::DEBUG)) {
      debug() << "M2 max " << param.chargeFracMax << " width "
              << param.chargeFracWidth << " 1st mask " << param.stripDistScale
              << " from outer " << param.fracOuter << " PitchScaling "
              << param.pitchScaling << endmsg;
    }
  }
  return sc;
}

StatusCode VeloRadDamageTool::loadDiffusion(const DeVeloSensor &sens) {
  unsigned int sNum = sens.sensorNumber();
  DiffusionParam &param = m_diffParamMap[sNum];
  if (sens.isPileUp()){
    param.useConst = true;
    param.constDiffusion = 1.;
    return StatusCode::SUCCESS;
  }

  // param is the current parameter (created if not existing already)
  //  DiffusionParam &param = m_diffParamMap[sNum];
  // note same SIMCOND condition as radiation damage but different
  // local container
  if (!(m_radConditionMap[sNum]->exists("DiffusionScaleAtRadii") ||
        m_radConditionMap[sNum]->exists("CapacitiveScaleAtRadii") ||
        m_radConditionMap[sNum]->exists("ConstDiffusion"))) {
    if (msgLevel(MSG::DEBUG)) {
      debug() << "Missing parameter DiffusionScale and ConstDiffusion, default = "
                 "1, sensor number: " << sNum << endmsg;
      if (!(m_radConditionMap[sNum]->exists("DiffusionScaleAtRadii")))
        debug() << " missing DiffusionScaleAtRadii" << endmsg;
      if (!(m_radConditionMap[sNum]->exists("CapacitiveScaleAtRadii")))
        debug() << " missing CapacitiveScaleAtRadii" << endmsg;
      if (!(m_radConditionMap[sNum]->exists("ConstDiffusion")))
        debug() << "missing ConstDiffusion" << endmsg;
    }
    return StatusCode::SUCCESS;
  }
  param.useConst = false;
  if (param.useConst) {
    param.constDiffusion =
        m_radConditionMap[sNum]->param<double>("DiffusionScale");
    // no spline for const output
    if (msgLevel(MSG::DEBUG))
      debug() << "Constant diffusion fraction" << param.constDiffusion
              << endmsg;
  } else {
    // so need spline
    std::vector<double> radii =
        m_radConditionMap[sNum]->param<std::vector<double> >("DiffusionRadii");
    std::vector<double> diffScale =
        m_radConditionMap[sNum]
            ->param<std::vector<double> >("DiffusionScaleAtRadii");
    std::vector<double> capScale =
        m_radConditionMap[sNum]
            ->param<std::vector<double> >("CapacitiveScaleAtRadii");

    if (radii.size() != diffScale.size()) {
      return Error(format("DiffusionRadii, DiffusionScaleAtRadii not consistent "
                          "for sensor %i",sNum),
                   StatusCode::FAILURE);
    }

    // outside range make a constant at the extremal value
    param.rMin = radii.front();
    param.rMax = radii.back();
    param.diffMin = diffScale.front();
    param.diffMax = diffScale.back();
    param.capMin = capScale.front();
    param.capMax = capScale.back();
    param.rVals = radii;
    param.diffVals = diffScale;
    param.diffSpline.reset(new GaudiMath::SimpleSpline(
        radii, diffScale, GaudiMath::Interpolation::Linear));
    param.capParam.first = capScale.at(0);
    param.capParam.second = capScale.at(1);

    if (msgLevel(MSG::DEBUG)) {
      debug() << "radius      diffScale capScale " << endmsg;
      for (unsigned int i = 0; i < radii.size(); ++i) {
        debug() << format("%5.2f   %5.2f  %5.2f ", radii[i], diffScale[i],
                          capScale[i]) << endmsg;
      }
    }
  }
  return StatusCode::SUCCESS;
}

void VeloRadDamageTool::dumpResponse(const DeVeloSensor& sens) const{
    verbose() << "response in 1mm steps from 0 to 55mm from function "
              <<endmsg;
    verbose() << "radius chargeFrac" <<endmsg;
    LHCb::MCHit hit;
    hit.setDisplacement(Gaudi::XYZVector(0.,0.,0.)); // explicitly set
    hit.setSensDetID(sens.sensorNumber()); // set the sensor
    for ( double r = 0 ; r < 55.5 ; r+=1.0 ){
      Gaudi::XYZPoint local(r,0.,0.); // want r in local frame so ...
      hit.setEntry(sens.localToGlobal(local));
      verbose() << format("%5.2f   %5.2f",r, chargeFrac( hit ) )
                << endmsg;
    }
}

double VeloRadDamageTool::chargeFrac(const LHCb::MCHit & hit) const {
  RadParam const &param = m_radParamMap.at(hit.sensDetID());
  if(param.useConst) return param.constantFrac; // in case spline not required
  const DeVeloSensor* sens=m_veloDet->sensor(hit.sensDetID());
  double localR = (sens->globalToLocal(hit.midPoint())).Rho();
  if( localR < param.rMin ) return param.responseMin; // less than spline range
  if( localR > param.rMax ) return param.responseMax; // more than spline range

  return param.responseSpline->eval(localR); // actually use the spline
}

StatusCode VeloRadDamageTool::m2CouplingFrac(const Gaudi::XYZPoint &point,
                                             const DeVeloSensor *sens,
                                             LHCb::VeloChannelID &chM2,
                                             double &fracInner,
                                             double &fracOuter) const {
  Warning(
      "VeloRadDamageTool::m2CouplingFrac without an explicit delta"
      " is no longer to be used",
      StatusCode::SUCCESS, 1).ignore();
  return this->m2CouplingFrac(point, Gaudi::XYZVector(0., 0., 0.3), sens, chM2,
                              fracInner, fracOuter);
}

double VeloRadDamageTool::diffusionScale(const DeVeloSensor *sens) const {
  RadParam const &param = m_radParamMap.find(sens->sensorNumber())->second;
  return param.diffusionScale;
}

double VeloRadDamageTool::diffusionScale(const DeVeloSensor *sens,
                                         const Gaudi::XYZPoint &point) const {
  DiffusionParam const &param = m_diffParamMap.at(sens->sensorNumber());
  if (param.useConst) {
    return param.constDiffusion;
  }
  double returnable = 0;
  double localR = (sens->globalToLocal(point)).Rho();
  // If we are outside the the evalueated radii, extrapolate linearly.
  if (localR < param.rMin)
    returnable = ((param.diffVals.at(1) - param.diffVals.at(0)) /
                  (param.rVals.at(1) - param.rVals.at(0))) *
                     (localR - param.rVals.at(0)) +
                 param.diffVals.at(0);  // less than spline range
  else if (localR > param.rMax)
    returnable = ((param.diffVals.at(3) - param.diffVals.at(2)) /
                  (param.rVals.at(3) - param.rVals.at(2))) *
                     (localR - param.rVals.at(2)) +
                 param.diffVals.at(2);  // more than spline range
  // std::cout<<"about to evaluate a spline!"<<std::endl;
  else
    returnable = param.diffSpline->eval(localR);  // actually use the spline

  if (returnable < 1.)
    return 1.;
  else
    return returnable;
}

float VeloRadDamageTool::capacitiveScale(const DeVeloSensor *sens,
                                          float pitch,
                                          unsigned int inner) const {
  DiffusionParam const &param = m_diffParamMap.at(sens->sensorNumber());
  if (param.useConst) {
    return 0.01;
  }
  float returnable = 0.;
  // If this is an R sensor, or inner or outer phi:
  if (sens->isR())
    returnable = param.capParam.first + param.capParam.second / pitch;
  else if (inner == 0)
    returnable = param.capParam.first;
  else
    returnable = param.capParam.second;

  if (returnable < 0.01)
    return 0.01;
  else
    return returnable;
}


double VeloRadDamageTool::reduceM2Coupling(const Gaudi::XYZVector &delta,
					   const DeVeloSensor *sens) const {
  RadParam const &param = m_radParamMap.find(sens->sensorNumber())->second;
  double eta = fabs(delta.eta()); // assume symmetric forward/backwards effects 
  if( eta <= param.etaMin ){
    return param.twoMetalFractionMin;
  }else if( eta >= param.etaMax ){
    return param.twoMetalFractionMax;
  }else{
    return param.couplingSpline->eval(eta);
  }
}

void VeloRadDamageTool::dumpM2Response(const DeVeloSensor& sens) const{
  verbose() << "response in 0.2 eta steps from 1 to 6 from function "
	    <<endmsg;
  verbose() << "eta " <<endmsg;
  for ( double eta = 1 ; eta < 6.01 ; eta += 0.2 ){
    double pz = tanh(eta);
    double px = sqrt(1-(pz*pz));
    Gaudi::XYZVector local(px,0.,pz); // want r in local frame so ...
    verbose() << format("%5.2f   %5.2f",eta, reduceM2Coupling(local,&sens))
	      << endmsg;
  }
}

StatusCode VeloRadDamageTool::m2CouplingFrac(const Gaudi::XYZPoint &point,
                                             const Gaudi::XYZVector &delta,
                                             const DeVeloSensor *sens,
                                             LHCb::VeloChannelID &chM2,
                                             double &fracInner,
                                             double &fracMain) const {

  // if the hit in an R sensor and close enough to a routing line
  // then charge is lost to the inner strip through the line
  RadParam const &param = m_radParamMap.find(sens->sensorNumber())->second;
  const DeVeloRType *sensR = dynamic_cast<const DeVeloRType *>(sens);
  if (!sensR)
    return StatusCode::FAILURE;  // phi hit, no effect simulated presently
  double distM2(0.), distStrip(0.);
  StatusCode sc = sensR->distToM2Line(point, chM2, distM2, distStrip);
  if (!sc) return sc;  // no coupling to M2 or hit out of active area
  double toStripFrac = 1.;
  if ((distStrip / param.stripDistScale) < 1.) {
    toStripFrac = distStrip / param.stripDistScale;
  }
  fracInner = param.chargeFracMax * toStripFrac *
              exp((-1. * distM2 * distM2) /
                  (2. * param.chargeFracWidth * param.chargeFracWidth));
  fracMain = fracInner * param.fracOuter;
  if (msgLevel(MSG::VERBOSE)) {
    verbose() << "global pos " << format("(%6.3f, %6.3f, %8.3f)", point.x(),
                                         point.y(), point.z()) << " dStrip "
              << distStrip << " dM2 " << distM2 << " innerF " << fracInner
              << " outerF " << fracMain << endmsg;
  }

  double deRate = reduceM2Coupling(delta, sens);

  LHCb::VeloChannelID mainChan;
  double fraction(0.), pitch(0.);
  sc = sensR->pointToChannel(point, mainChan, fraction, pitch);
  if (!sc) return sc;  //  hit out of active area

  // i.e. (ratio to inner pitch)^2 * pitch coupling effect
  deRate *= 1 + ((pitch / 0.040) * (pitch / 0.040)) * param.pitchScaling;

  fracMain *= deRate;

  if (msgLevel(MSG::VERBOSE)) {
    verbose() << "eta " << delta.eta() << " disp " << delta << " deRate "
              << deRate << " pitch " << pitch << " fInner " << fracInner
              << " fMain " << fracMain << endmsg;
  }
  return sc;
}
