#pragma once

// Include files

// from Gaudi
#include "GaudiKernel/IAlgTool.h"

namespace LHCb{
  class MCHit;
  class VeloChannelID;
}
class DeVeloSensor;

/** @class IRadDamageTool IRadDamageTool.h
 *  Interface class for reduction in effective charge with position
 *  initially for VELO radiation damage simulation
 *
 *  @author David Hutchcroft
 *  @date   2011-3-31
 */

// Declaration of  the interface ID
static const InterfaceID IID_IRadDamageTool( "IRadDamageTool", 1, 0 );

class IRadDamageTool : virtual public IAlgTool {
public:
  /// Static access to interface id
  static const InterfaceID& interfaceID() { return IID_IRadDamageTool; }

  /** fraction of charge seen due to radiation damage
  * @param point : position (global frame) of hit
  * @return fraction of charge after rad damage
  */
  virtual double chargeFrac(const LHCb::MCHit &hit) const = 0;

  /** fraction of charge seen due to radiation damage
  * @param point : position (global frame) of hit
  * @param sens : DeVeloSensor of the point
  * @param chM2 : the channel of the inner strip
  * @param fracInner : fraction of charge to inner strip
  * @param fracMain : fraction of charge from main strip
  * @return Success if coupled to inner strip
  */
  virtual StatusCode m2CouplingFrac(const Gaudi::XYZPoint &point,
                                    const DeVeloSensor *sens,
                                    LHCb::VeloChannelID &chM2,
                                    double &fracInner,
                                    double &fracMain) const = 0;

  /** fraction of charge seen due to radiation damage
  * @param point : position (global frame) of hit
  * @param delta : Vector from entry to exit (for slope corrections)
  * @param sens : DeVeloSensor of the point
  * @param chM2 : the channel of the inner strip
  * @param fracInner : fraction of charge to inner strip
  * @param fracMain : fraction of charge from main strip
  * @return Success if coupled to inner strip
  */
  virtual StatusCode m2CouplingFrac(const Gaudi::XYZPoint &point,
                                    const Gaudi::XYZVector &delta,
                                    const DeVeloSensor *sens,
                                    LHCb::VeloChannelID &chM2,
                                    double &fracInner,
                                    double &fracMain) const = 0;

  /** Amount to scale the default charge diffusion by in each sensor
   * @param sens : DeVeloSensor of the hit
   * @return value to scale diffusion by (1.0 is no change)
   */
  virtual double diffusionScale(const DeVeloSensor *sens) const = 0;

  virtual double diffusionScale(const DeVeloSensor *sens,
                                const Gaudi::XYZPoint &point) const = 0;

  /** Amount to scale the default capacitive coupling by in each sensor
   * @param sens : DeVeloSensor of the hit
   * @param rho : rough r position on the sensor
   * @return capacitive coupling scale factor
   */
  virtual float capacitiveScale(const DeVeloSensor *sens, float pitch,
                                 unsigned int inner = 0) const = 0;
};

