// Include files

// local
#include "VeloCCEConfigTool.h"

//-----------------------------------------------------------------------------
// Implementation file for class : VeloCCEConfigTool
//
// 2016-03-04 : Jon Harrison, based on code by David Hutchcroft
//-----------------------------------------------------------------------------

DECLARE_COMPONENT( VeloCCEConfigTool )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
VeloCCEConfigTool::VeloCCEConfigTool(const std::string& type,
                                     const std::string& name,
                                     const IInterface* parent)
  : GaudiTool ( type, name , parent )
{
  declareInterface<IVeloCCEConfigTool>(this);
}


StatusCode VeloCCEConfigTool::initialize(){
  StatusCode sc = GaudiTool::initialize();
  if (sc.isFailure()) return Error("Failed to initialize", sc);

  std::vector<std::string> conditions = {"Conditions/HardwareProperties/Velo/CCEscans",
			       "Conditions/HardwareProperties/Velo/BadStrips"}; 

  //Set parameters from CondDB
  for (unsigned int i = 0; i < conditions.size(); i++)
  {
    if( !existDet<DataObject>(detSvc(),conditions[i]) ){
      Warning("VELO CCE scan parameters not in conditions DB at " + conditions[i],
              StatusCode::SUCCESS).ignore();

      CCEParam param;

      return StatusCode::SUCCESS;
    }
  }

  for (unsigned int i = 0; i < conditions.size(); i++)
  {
    if( !existDet<Condition>(detSvc(),conditions[i]+"/Default") ){
      return Warning("Condition directory exists but missing condition " +
                       conditions[i]+"/Default",StatusCode::FAILURE);
    }
    else{
      m_CCEscanConditions[i] = get<Condition>(detSvc(),conditions[i]+"/Default");
      registerCondition(conditions[i], &VeloCCEConfigTool::i_loadCondition);
      if(msgLevel( MSG::VERBOSE ))
        verbose() << "Registered " << conditions[i]
                  << " into m_CCEscanConditions "
                  << m_CCEscanConditions[i] << endmsg;
    }  
  } 
  sc = i_loadCondition();
  sc = runUpdate();
  if(!sc) return sc;

  return StatusCode::SUCCESS;
}

StatusCode VeloCCEConfigTool::i_loadCondition(){

  param.voltage.clear();
  param.sensors.clear();
  param.badStripsR.clear();
  param.badStripsPhi.clear();
  
  //Set CCE scan steps
  int nSteps = m_CCEscanConditions[0]->param<int>("nSteps");

  for (int cceStep = 1; cceStep <= nSteps; cceStep++) 
  {
    std::string voltageString = format("Voltage%d",cceStep);
    std::string sensorString = format("KilledSensors%d",cceStep);
    param.voltage.push_back(m_CCEscanConditions[0]->param<int>(voltageString));
    param.sensors.push_back(m_CCEscanConditions[0]->param<std::vector<int> >(sensorString));
    if(msgLevel( MSG::DEBUG )) debug() 
      << "Voltage for CCE step " << cceStep << " = " << param.voltage.back()
      << ", killed sensors for CCE step " << cceStep << " = " << param.sensors.back() 
      << endmsg;
    else if(msgLevel( MSG::VERBOSE )) verbose() 
      << "Voltage for CCE step " << cceStep << " = " << param.voltage.back()
      << ", killed sensors for CCE step " << cceStep << " = " << param.sensors.back() 
      << endmsg;
  }

  //Set bad strips 
  for (int sensorNum = 0; sensorNum < 42; sensorNum++) 
  {
    std::string badStripRString = format("BadStripsSensorR%d",sensorNum);
    std::string badStripPhiString = format("BadStripsSensorPhi%d",sensorNum);
    param.badStripsR.push_back(m_CCEscanConditions[1]->param<std::vector<int> >(badStripRString));
    param.badStripsPhi.push_back(m_CCEscanConditions[1]->param<std::vector<int> >(badStripPhiString));
    if(msgLevel( MSG::DEBUG )) debug() 
      << "Bad strips for sensor R" << sensorNum << " = " << param.badStripsR.back() 
      << "Bad strips for sensor Phi" << sensorNum << " = " << param.badStripsPhi.back() 
      << endmsg;
    else if(msgLevel( MSG::VERBOSE )) verbose() 
      << "Bad strips for sensor R" << sensorNum << " = " << param.badStripsR.back() 
      << "Bad strips for sensor Phi" << sensorNum << " = " << param.badStripsPhi.back() 
      << endmsg;
  }
  return StatusCode::SUCCESS;
}

int VeloCCEConfigTool::findKilledSensors(int &CCEstep, std::vector<int> &killSensorList) const{
  verbose() << "Finding CCE scan parameters for CCE step " << CCEstep << endmsg;

  if (CCEstep > 0 && param.sensors.size() >= (unsigned)CCEstep && param.voltage.size() >= (unsigned)CCEstep)
  {
     killSensorList = param.sensors[CCEstep-1];
     return param.voltage[CCEstep-1];
  }
  else
  {
     killSensorList = std::vector<int> ();
     return -1;
  }
}

void VeloCCEConfigTool::findBadStrips(unsigned int &sensorNum, std::vector<int> &badStripList) const{
  verbose() << "Finding bad strips for CCE config for sensor " << sensorNum << endmsg;
 
  //Returns R sensor unless sensorNum > 41 
  if (sensorNum < 42 && param.badStripsR.size() > sensorNum)
  {
     badStripList = param.badStripsR[sensorNum];
  }
  else if (sensorNum > 63 && param.badStripsPhi.size() > sensorNum-64)
  {
     badStripList = param.badStripsPhi[sensorNum-64];
  }
  else
  {
     badStripList = std::vector<int> ();
  }
}
