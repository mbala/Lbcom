################################################################################
# Package: VeloTools
################################################################################
gaudi_subdir(VeloTools v4r3)

gaudi_depends_on_subdirs(DAQ/Tell1Kernel
                         Det/VeloDet
                         Event/DigiEvent
                         Event/TrackEvent
                         GaudiAlg
                         GaudiKernel
                         Kernel/LHCbKernel)

# hide warnings from some external projects
find_package(Boost)
find_package(ROOT)
find_package(CLHEP)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${CLHEP_INCLUDE_DIRS}
                           ${ROOT_INCLUDE_DIRS})

gaudi_add_module(VeloTools
                 src/*.cpp
                 INCLUDE_DIRS DAQ/Tell1Kernel Event/DigiEvent
                 LINK_LIBRARIES VeloDetLib TrackEvent GaudiAlgLib GaudiKernel LHCbKernel)

