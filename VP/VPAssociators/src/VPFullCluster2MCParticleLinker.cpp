#include "Event/MCHit.h"
#include "Event/VPDigit.h"

#include <Associators/Location.h>

#include "VPFullCluster2MCParticleLinker.h"

DECLARE_COMPONENT(VPFullCluster2MCParticleLinker)

//=============================================================================
// Constructor
//=============================================================================
VPFullCluster2MCParticleLinker::VPFullCluster2MCParticleLinker(const std::string& name,
                                                       ISvcLocator* svcLoc)
: Linker(name, svcLoc,
         {KeyValue{"ClusterLocation", LHCb::VPFullClusterLocation::Default},
          KeyValue{"VPDigit2MCParticleLinksLocation",
                   Links::location(LHCb::VPDigitLocation::Default)}},
         KeyValue{"OutputLocation", Links::location(LHCb::VPFullClusterLocation::Default)})
{
}

//=============================================================================
// Main operator
//=============================================================================
LHCb::LinksByKey VPFullCluster2MCParticleLinker::operator()(const std::vector<LHCb::VPFullCluster>& clusters,
                                                        const LHCb::LinksByKey& bareDigitLinks) const
{
   // Get the association table between digits and particles.
   auto digitLinks = inputLinks<LHCb::MCParticle>(bareDigitLinks);

   auto output = OutputLinks<LHCb::VPFullCluster,LHCb::MCParticle>{};
   // Loop over the clusters.
   for (const LHCb::VPFullCluster& cluster : clusters) {
      std::map<const LHCb::MCParticle*, double> particleMap;
      // Get the pixels in the cluster, not available anymore in VPFullClusters
      const auto& pixels = cluster.pixels();
      double sum = 0.;
      for (const auto& pixel : pixels) {
         for (const auto& entry : digitLinks.from(pixel)) {
	   const double weight = entry.weight();
	   particleMap[entry.to()] += weight;
	   sum += weight;
         }
      }
      if (sum < 1.e-2) continue;
      for (const auto& entry : particleMap) {
         const double weight = entry.second / sum;
	 output.link(cluster.channelID(), entry.first, weight);
      }
   }
   return output;
}
