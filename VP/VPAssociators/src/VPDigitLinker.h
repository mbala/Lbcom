#ifndef VPDIGITLINKER_H
#define VPDIGITLINKER_H 1

// LHCb
#include <Event/MCParticle.h>
#include <Event/LinksByKey.h>
#include <Event/VPCluster.h>

// Associators
#include <Associators/Linker.h>

/** @class VPDigitLinker VPDigitLinker.h
 *  This algorithm creates association tables between the channel IDs of
 *  the VP digits and the corresponding MC hits and particles.
 *
 */
class VPDigitLinker
   : public Gaudi::Functional::MultiLinker<std::tuple<LHCb::LinksByKey, LHCb::LinksByKey>
                                           (const LHCb::VPDigits&, const LHCb::MCVPDigits&)>
{
public:

   /// Standard constructor
   VPDigitLinker(const std::string& name, ISvcLocator* pSvcLocator);

   std::tuple<LHCb::LinksByKey, LHCb::LinksByKey>
   operator()(const LHCb::VPDigits&, const LHCb::MCVPDigits&) const override;

};

#endif
