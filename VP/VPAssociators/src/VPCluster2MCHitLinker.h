#ifndef VPCLUSTER2MCHITLINKER_H
#define VPCLUSTER2MCHITLINKER_H 1

// LHCb
#include <Event/MCHit.h>
#include <Event/MCParticle.h>
#include <Event/LinksByKey.h>
#include <Event/VPCluster.h>

// Associators
#include <Associators/Linker.h>

/** @class VPClusterLinker VPClusterLinker.h
 * These algorithms create association tables between the VP clusters
 * and the corresponding MC hits, based on the association tables for
 * individual pixels produced by VPDigitLinker.
 */

class VPCluster2MCHitLinker
   : public Gaudi::Functional::Linker<LHCb::LinksByKey(const LHCb::VPClusters&,
                                                       const LHCb::LinksByKey&)>
{
public:

   //Standard constructor
   VPCluster2MCHitLinker(const std::string& name, ISvcLocator* pSvcLocator);

   virtual LHCb::LinksByKey operator()(const LHCb::VPClusters& clusters,
                                       const LHCb::LinksByKey& digitLinks) const override; // < Algorithm execution

};

#endif
