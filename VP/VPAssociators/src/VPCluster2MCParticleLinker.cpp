#include "Event/MCHit.h"
#include "Event/VPDigit.h"

#include <Associators/Location.h>

#include "VPCluster2MCParticleLinker.h"

DECLARE_COMPONENT(VPCluster2MCParticleLinker)

//=============================================================================
// Constructor
//=============================================================================
VPCluster2MCParticleLinker::VPCluster2MCParticleLinker(const std::string& name,
                                                       ISvcLocator* svcLoc)
: Linker(name, svcLoc,
         {KeyValue{"ClusterLocation", LHCb::VPClusterLocation::Light},
          KeyValue{"VPDigit2MCParticleLinksLocation",
                   Links::location(LHCb::VPDigitLocation::Default)}},
         KeyValue{"OutputLocation", Links::location(LHCb::VPClusterLocation::Light)})
{
}

//=============================================================================
// Initialize
//=============================================================================
StatusCode VPCluster2MCParticleLinker::initialize() 
{
  StatusCode sc = Linker::initialize();
  if (sc.isFailure()) return sc;

  warning()<<"VPLightClusters no longer contain a vector of pixels contributing to the cluster, for performance reasons"<<endmsg;
  warning()<<"Efficiencies will not be 100% correct."<<endmsg;
  warning()<<"Use VPFullClusters and VPFullCluster2MCParticleLinker to obtain the correct efficiencies."<<endmsg;

  return sc;
}

//=============================================================================
// Main operator
//=============================================================================
LHCb::LinksByKey VPCluster2MCParticleLinker::operator()(const std::vector<LHCb::VPLightCluster>& clusters,
                                                        const LHCb::LinksByKey& bareDigitLinks) const
{
   // Get the association table between digits and particles.
   auto digitLinks = inputLinks<LHCb::MCParticle>(bareDigitLinks);

   auto output = OutputLinks<LHCb::VPLightCluster,LHCb::MCParticle>{};
   // Loop over the clusters.
   for (const LHCb::VPLightCluster& cluster : clusters) {
      std::map<const LHCb::MCParticle*, double> particleMap;
      // Get the pixels in the cluster, not available anymore in VPLightClusters.
      // const auto& pixels = cluster.pixels();
      // double sum = 0.;
      // for (const auto& pixel : pixels) {
      //    for (const auto& entry : digitLinks.from(pixel)) {
      //       const double weight = entry.weight();
      //       particleMap[entry.to()] += weight;
      //       sum += weight;
      //    }
      // }
      const auto& centralpixel = cluster.channelID();
      double sum = 0.;
      for( const auto& entry : digitLinks.from(centralpixel)){
	const double weight = entry.weight();
	particleMap[entry.to()]+=weight;
	sum+= weight;
      }
      if (sum < 1.e-2) continue;
      for (const auto& entry : particleMap) {
         const double weight = entry.second / sum;
         output.link(cluster.channelID(), entry.first, weight);
      }
   }
   return output;
}
