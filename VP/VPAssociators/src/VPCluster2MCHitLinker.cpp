#include "Event/MCHit.h"
#include "Event/VPDigit.h"

#include <Associators/Location.h>

#include "VPCluster2MCHitLinker.h"

DECLARE_COMPONENT(VPCluster2MCHitLinker)

//=============================================================================
// Constructor
//=============================================================================
VPCluster2MCHitLinker::VPCluster2MCHitLinker(const std::string& name,
                                             ISvcLocator* pSvcLocator)
: Linker(name, pSvcLocator,
         {KeyValue{"ClusterLocation", LHCb::VPClusterLocation::Default},
          KeyValue{"VPDigit2MCHitLinksLocation", Links::location(LHCb::VPDigitLocation::Default + "2MCHits")}},
         KeyValue{"OutputLocation", Links::location(LHCb::VPClusterLocation::Default + "2MCHits")})
{
}

//=============================================================================
// Main operator
//=============================================================================
LHCb::LinksByKey VPCluster2MCHitLinker::operator()(const LHCb::VPClusters& clusters,
                                                   const LHCb::LinksByKey& bareDigitLinks) const
{
   // Get the association table between digits and hits.
   auto digitLinks = inputLinks<LHCb::VPDigit, LHCb::MCHit>(bareDigitLinks);

   // Create an association table between hits and clusters.
   auto output = outputLinks<LHCb::MCHit>();
   // Loop over the clusters.

   for (const LHCb::VPCluster* cluster : clusters) {
      std::map<const LHCb::MCHit*, double> hitMap;
      // Get the pixels in the cluster.
      auto pixels = cluster->pixels();
      double sum = 0.;
      //Loop over all pixels in a given cluster
      for (const auto& pixel : pixels) {
         //Get the MCHit pointer from the associator
         auto range = digitLinks.from(pixel);
         for (const auto& entry : range) {
            const double weight = entry.weight();
            hitMap[entry.to()] += weight;
            sum += weight;
         }
         if(msgLevel(MSG::VERBOSE)) {
            verbose() << "Got " << range.size() << " MCHits from associator." << endmsg;
         }
      }
      if (sum < 1.e-2) continue;

      for (const auto& entry : hitMap) {
         const double weight = entry.second / sum;
         output.link(cluster, entry.first, weight);
      }
   }
   return output;
}
