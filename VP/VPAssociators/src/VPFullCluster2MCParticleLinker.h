#ifndef VPFULLCLUSTER2MCPARTICLELINKER_H
#define VPFULLCLUSTER2MCPARTICLELINKER_H 1

// LHCb
#include <Event/MCParticle.h>
#include <Event/LinksByKey.h>
#include <Event/VPFullCluster.h>

// Associators
#include <Associators/Linker.h>

/** @class VPFullClusterLinker VPFullClusterLinker.h
 * These algorithms create association tables between the VP clusters
 * and the corresponding MC particles, based on the association tables
 * for individual pixels produced by VPDigitLinker.
 */

class VPFullCluster2MCParticleLinker
   : public Gaudi::Functional::Linker<LHCb::LinksByKey(const std::vector<LHCb::VPFullCluster>&,
                                                       const LHCb::LinksByKey&)>
{
public:

   //Standard constructor
   VPFullCluster2MCParticleLinker(const std::string& name, ISvcLocator* pSvcLocator);

   LHCb::LinksByKey operator()(const std::vector<LHCb::VPFullCluster>& clusters,
                               const LHCb::LinksByKey& digitLinks) const override; // < Algorithm execution

};

#endif
