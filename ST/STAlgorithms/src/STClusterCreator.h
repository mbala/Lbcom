#ifndef STCLUSTERCREATOR_H
#define STCLUSTERCREATOR_H 1

#include "Kernel/STCommonBase.h"
#include "Event/STDigit.h"
#include "Event/STCluster.h"
#include "Kernel/ISTReadoutTool.h"

class DeSTSDetector;
class DeSTSector;
struct ISTClusterPosition;

/** @class STClusterCreator STClusterCreator.h
 *
 *  Class for clustering in the ST tracker
 *
 *  @author M.Needham
 *  @date   07/03/2002
 */

template <class IReadoutTool = ISTReadoutTool>
class STClusterCreatorT : public ST::CommonBase<GaudiAlgorithm, IReadoutTool> {

public:

  // Constructors and destructor
  STClusterCreatorT( const std::string& name, ISvcLocator* pSvcLocator);

  // IAlgorithm members
  StatusCode initialize() override;
  StatusCode execute() override;

private:

  StatusCode createClusters( const LHCb::STDigits* digitsCont,
                             LHCb::STClusters* clustersCont) const ;

  bool keepClustering( const LHCb::STDigit* firstDigit,
                       const LHCb::STDigit* secondDigit,
                       const DeSTSector* aSector ) const;

  bool sameBeetle( const LHCb::STChannelID firstChan,
                   const LHCb::STChannelID secondChan ) const;

  bool aboveDigitSignalToNoise( const LHCb::STDigit* aDigit,
                                const DeSTSector* aSector ) const;
  bool aboveClusterSignalToNoise( const double charge,
                                  const DeSTSector* sector ) const;
  bool hasHighThreshold(const double charge, const DeSTSector* aSector) const;

  double neighbourSum( LHCb::STDigits::const_iterator start,
                       LHCb::STDigits::const_iterator stop,
                       const LHCb::STDigits* digits ) const;

  LHCb::STCluster::ADCVector strips(const SmartRefVector<LHCb::STDigit>&
                                    clusteredDigits,
                                    const LHCb::STChannelID closestChan) const;

  StatusCode loadCutsFromConditions();

  Gaudi::Property<bool> m_forceOptions{this, "forceOptions", false};
  Gaudi::Property<double> m_digitSig2NoiseThreshold{this, "DigitSignal2Noise", 3.0};
  Gaudi::Property<double> m_clusterSig2NoiseThreshold{this, "ClusterSignal2Noise", 4.0};
  Gaudi::Property<double> m_highThreshold{this, "HighSignal2Noise", 10.0};
  Gaudi::Property<std::string> m_positionToolName{this, "PositionTool", "STOnlinePosition"};
  Gaudi::Property<int> m_outputVersion{this, "OutputVersion", 1};
  Gaudi::Property<int> m_maxSize{this, "Size", 4};
  Gaudi::Property<bool> m_byBeetle{this, "ByBeetle", true};
  Gaudi::Property<std::string> m_spillName{this, "Spill", "Central"};

  std::string  m_conditionLocation;

  typedef std::map<const DeSTSector*,unsigned int> CutMap;

  mutable CutMap m_clusterSig2NoiseCut;
  mutable CutMap m_highSig2NoiseCut;

  std::string m_inputLocation;
  std::string m_outputLocation;
  LHCb::STCluster::Spill m_spill;


  ISTClusterPosition* m_positionTool = nullptr;

};

// Declaration of the backward compatible STCLusterMonitor class (not templated for the original ST case)
using STClusterCreator = STClusterCreatorT<>;

#endif // STCLUSTERSCREATOR_H
