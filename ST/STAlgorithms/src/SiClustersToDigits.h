#ifndef SICLUSTERSTODIGITS_H
#define SICLUSTERSTODIGITS_H 1


/** @class SiClustersToDigits
 *
 *  simple class for stripping digits from clusters
 *
 *  @author M.Needham
 *  @date   27/02/2009
 */

#include "SiClusterTraits.h"

template <class TYPE>
class SiClustersToDigits :public SiClusterTraits<TYPE>::BASEALG {

public:
  
  /// Constructor
  SiClustersToDigits( const std::string& name, ISvcLocator* pSvcLocator); 

  /// IAlgorithm members
  StatusCode execute() override;

private:

  typename SiClusterTraits<TYPE>::DIGIT* createDigit(const unsigned int value);

  void addNeighbours(TYPE* cluster, 
                     typename SiClusterTraits<TYPE>::DIGIT::Container* digitCont); 

  bool m_addNeighbourInfo;
  std::string m_inputLocation;
  std::string m_outputLocation;


};

#endif // SiClustersToDigits
