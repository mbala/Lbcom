#ifndef VELOCLUSTERSTOLITE_H
#define VELOCLUSTERSTOLITE_H 1

/** @class VeloClustersToLite
 *
 *  simple class for stripping liteCluster from clusters
 *  ST specialization
 *
 *  @author M.Needham
 *  @date   02/10/2008
 */

// Gaudi
#include "Event/VeloCluster.h"
#include "Event/VeloLiteCluster.h"
#include "Kernel/STAlgBase.h"

#include "SiClustersToLite.h"
#include "SiClusterTraits.h"
typedef SiClustersToLite<LHCb::VeloCluster> VeloClustersToLite;
#include "SiClustersToLite.icpp"

/// specialized constructor
inline SiClustersToLite<LHCb::VeloCluster>::SiClustersToLite(const std::string& name,
                                                             ISvcLocator* pSvcLocator):
Gaudi::Functional::MultiTransformer<
    std::tuple<typename SiClusterTraits<LHCb::VeloCluster>::LITECONT,
               LHCb::VeloDecodeStatus> (const typename SiClusterTraits<LHCb::VeloCluster>::CLUSCONT&),
    SiClusterBaseClassTraits<LHCb::VeloCluster> >(name,
                                                  pSvcLocator,
                                                  { KeyValue{ "inputLocation", LHCb::VeloClusterLocation::Default } },
                                                  { KeyValue{ "outputLocation", LHCb::VeloLiteClusterLocation::Default },
                                                    KeyValue{ "decstatusLocation", LHCb::VeloLiteClusterLocation::Default + "DecStatus" } } )
{ }

// specalisation just for the VELO to create the DecStatus object too
std::tuple<typename SiClusterTraits<LHCb::VeloCluster>::LITECONT, LHCb::VeloDecodeStatus>
SiClustersToLite<LHCb::VeloCluster>::operator() (const typename SiClusterTraits<LHCb::VeloCluster>::CLUSCONT &clusterCont) const {
  SiClusterTraits<LHCb::VeloCluster>::LITECONT fCont;
  LHCb::VeloDecodeStatus decStatus;
  std::unordered_set<unsigned int> usedSensors;
  fCont.reserve(clusterCont.size());
  for ( auto clus: clusterCont  ) {
    fCont.push_back(clus->liteCluster());
    usedSensors.insert(clus->liteCluster().channelID().sensor());
  } // loop clusters
  for(auto s : usedSensors){
    decStatus.setDecoderState(s, true); // assume clusters are from good banks
  }
  return std::make_tuple(std::move(fCont), std::move(decStatus));
}

#endif // VeloClustersToLite

