#ifndef SICLUSTERSTOLITE_H
#define SICLUSTERSTOLITE_H 1


/** @class SiClustersToLite
 *
 *  simple class for stripping liteCluster from clusters
 *
 *  @author M.Needham
 *  @date   11/02/2009
 */

#include "GaudiAlg/Transformer.h"
#include "GaudiAlg/FunctionalUtilities.h"
#include "SiClusterTraits.h"
#include "VeloEvent/VeloDecodeStatus.h"

template <class TYPE>
using SiClusterBaseClassTraits = Gaudi::Functional::Traits::BaseClass_t<typename SiClusterTraits<TYPE>::BASEALG>;

template <class TYPE>
struct SiClustersToLite : Gaudi::Functional::Transformer<
    typename SiClusterTraits<TYPE>::LITECONT(const typename SiClusterTraits<TYPE>::CLUSCONT&),
    SiClusterBaseClassTraits<TYPE> > {

  /// Constructor
  SiClustersToLite( const std::string& name, ISvcLocator* pSvcLocator);

  /// operator()
  typename SiClusterTraits<TYPE>::LITECONT operator()
    (const typename SiClusterTraits<TYPE>::CLUSCONT&) const override;

};

template <>
struct SiClustersToLite<LHCb::VeloCluster> : Gaudi::Functional::MultiTransformer<
    std::tuple<typename SiClusterTraits<LHCb::VeloCluster>::LITECONT,
               LHCb::VeloDecodeStatus>(const typename SiClusterTraits<LHCb::VeloCluster>::CLUSCONT&),
    SiClusterBaseClassTraits<LHCb::VeloCluster> > {

  /// Constructor
  SiClustersToLite( const std::string& name, ISvcLocator* pSvcLocator);

  /// operator()
  std::tuple<typename SiClusterTraits<LHCb::VeloCluster>::LITECONT,
             LHCb::VeloDecodeStatus> operator()
    (const typename SiClusterTraits<LHCb::VeloCluster>::CLUSCONT&) const override;

};

#endif // SiClustersToLite
