#ifndef STCLUSTERMONITOR_H 
#define STCLUSTERMONITOR_H 1

// Include files
#include "Kernel/STCommonBase.h"
#include "Kernel/IUTReadoutTool.h"
#include "Event/ODIN.h"

namespace LHCb{
  class STCluster;
}
class DeSTSector;


// Boost accumulator classes to store MPVs of sectors
#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics/stats.hpp>
#include <boost/accumulators/statistics/median.hpp>
#include <boost/accumulators/statistics/mean.hpp>

// Use root histos to get MPV for sectors (in absence of something better)
class TH1D;

/** @class STClusterMonitorT STClusterMonitor.h
 *  
 *
 *  @author Mark Tobin
 *  @date   2009-03-10
 */

/// Define some typedefs for boost accumulators
namespace ST {

  // Median of distribution
  typedef boost::accumulators::accumulator_set
  <double,boost::accumulators::stats<boost::accumulators::tag::median(boost::accumulators::with_p_square_quantile) > > 
  MedianAccumulator;

  // Mean of distribution
  typedef boost::accumulators::accumulator_set
  <double,boost::accumulators::stats<boost::accumulators::tag::mean(boost::accumulators::immediate) > > 
  MeanAccumulator;

}

namespace ST
{
  template<class IReadoutTool = ISTReadoutTool>
  class STClusterMonitorT : public CommonBase<GaudiHistoAlg, IReadoutTool> {
  public:

    using Base = CommonBase<GaudiHistoAlg, IReadoutTool>;
    /// Standard constructor
    STClusterMonitorT( const std::string& name, ISvcLocator* pSvcLocator );

    StatusCode initialize() override;    ///< Algorithm initialization
    StatusCode execute() override;    ///< Algorithm execution
    
  private:
    /// This is the algorithm which plots cluster ADCs vs Sampling time
    void monitorClusters();

    /// Fill basic histograms
    void fillHistograms(const LHCb::STCluster* cluster, std::vector<unsigned int>& nClustersPerTELL1);

    /// Fill detailed histograms
    void fillDetailedHistograms(const LHCb::STCluster* cluster);

    /// Fill the hitmaps when required.
    void fillClusterMaps(const LHCb::STCluster* cluster);

    /// Fill the cluster mpv map per sector
    void fillMPVMap(const DeSTSector* sector, double charge);

    std::string m_clusterLocation; ///< Input location for clusters
  
    /// Toggles to turn plots off/on
    Gaudi::Property<bool> m_plotBySvcBox {this, "ByServiceBox", false, "Plot by Service Box"};
    Gaudi::Property<bool> m_plotByDetRegion {this, "ByDetectorRegion", false, "Plot by unique detector region"};
    Gaudi::Property<bool> m_plotByPort {this, "ByPort", false, "Plot number of clusters/tell1 by vs port"};
    Gaudi::Property<bool> m_hitMaps {this, "HitMaps", false, "True if cluster maps are to be shown"};

    /// Cuts on the data quality
    Gaudi::Property<double> m_chargeCut {this, "ChargeCut", 0, "Cut on charge"};
    Gaudi::Property<unsigned int> m_minNClusters {this, "MinTotalClusters", 0, "Cut on minimum number of clusters in the event"};
    Gaudi::Property<unsigned int> m_maxNClusters {this, "MaxTotalClusters", std::numeric_limits<unsigned int>::max(), "Cut on maximum number of clusters in the event"};
    Gaudi::Property<double> m_minMPVCharge {this, "MinMPVCharge", 0, "Minimum charge for calculation on MPV"};

    Gaudi::Property<unsigned int> m_overFlowLimit{this, "OverflowLimit", 5000, "Overflow limit for number of clusters in event"};
    Gaudi::Property<unsigned int> m_resetRate{this, "ResetRate", 1000, "Reset rate for MPV calculation"};

    mutable Gaudi::Accumulators::Counter<> m_eventsCount{this, "Number of events"};

    unsigned int m_nTELL1s; ///< Number of TELL1 boards expected.

    /// Book histograms
    void bookHistograms();

    // filled in monitor clusters
    AIDA::IHistogram1D* m_1d_nClusters = nullptr;///< Number of clusters
    AIDA::IHistogram1D* m_1d_nClusters_gt_100 = nullptr;///< Number of clusters (N > 100)
    AIDA::IHistogram1D* m_1d_nClustersVsTELL1 = nullptr;///< Number of clusters per TELL1
    AIDA::IProfile1D* m_prof_nClustersVsTELL1 = nullptr;///< Number of clusters per TELL1
    AIDA::IHistogram2D* m_2d_nClustersVsTELL1 = nullptr;///< Number of clusters per TELL1

    AIDA::IHistogram1D* m_1d_nClustersVsTELL1Links = nullptr;///< Number of clusters per TELL1 (split into links)
    AIDA::IHistogram1D* m_1d_nClustersVsTELL1Sectors = nullptr;///< Number of clusters per TELL1 (split into sectors)

    AIDA::IHistogram1D* m_1d_nClusters_overflow = nullptr;///< Number of clusters where last bin contains overflow info

    // filled in fillHistograms
    AIDA::IHistogram1D* m_1d_ClusterSize            = nullptr; ///< Cluster Size
    AIDA::IHistogram2D* m_2d_ClusterSizeVsTELL1     = nullptr; ///< Cluster Size vs TELL1
    AIDA::IHistogram2D* m_2d_STNVsTELL1             = nullptr; ///< Signal to Noise vs TELL1
    AIDA::IHistogram2D* m_2d_ChargeVsTELL1          = nullptr; ///< Cluster Charge vs TELL1
    AIDA::IHistogram2D* m_2d_ClustersPerPortVsTELL1 = nullptr; ///< Clusters per port vs TELL1

    AIDA::IHistogram1D* m_1d_totalCharge            = nullptr; ///< Total charge in cluster

    /// Plots by service box
    std::map<std::string, AIDA::IHistogram1D*> m_1ds_chargeByServiceBox;
    /// Plots by detector region
    std::map<std::string, AIDA::IHistogram1D*> m_1ds_chargeByDetRegion;

    /// Hitmaps
    AIDA::IHistogram2D* m_2d_hitmap           = nullptr; ///< Cluster hitmap
    AIDA::IProfile1D* m_prof_sectorMPVs       = nullptr; ///< Sector MPV vs arbitrary sector number 
    AIDA::IProfile1D* m_prof_sectorTruncMean1 = nullptr; ///< Sector MPV calculated using truncated mean losing first and last 15%
    AIDA::IProfile1D* m_prof_sectorTruncMean2 = nullptr; ///< Sector MPV calculated using truncated mean 1st 70% of ADC distribution
    AIDA::IProfile1D* m_prof_sectorBinnedMPV  = nullptr; ///< Sector MPV vs arbitrary sector number 
    AIDA::IHistogram2D* m_2d_sectorMPVs       = nullptr; ///< Sector MPV maps
    AIDA::IHistogram2D* m_2d_sectorMPVsNorm   = nullptr; ///< Use to normalize the MPV distribution in the history mode of the presenter

    unsigned int m_nSectors;///< Number of sectors (ie hybrids)
    unsigned int m_nBeetlePortsPerSector; ///< Number of beetle ports per sector
    static const unsigned int m_nBeetlePortsPerTTSector=16u;///< Number of bins per TT sector in the hitmap (beetle ports)
    static const unsigned int m_nBeetlePortsPerUTSector=16u;///< Number of bins per UT sector in the hitmap (beetle ports)
    static const unsigned int m_nBeetlePortsPerITSector=12u;///< Number of bins in each IT sector (beetle ports)
    unsigned int m_nSectorsPerTELL1;
    /// Accumulation of statistics for the MPV per sector
    std::map<const unsigned int,TH1D*> m_1ds_chargeBySector;
    std::map<const unsigned int, std::vector<double> > m_chargeBySector;

    /// Contains the bin corresponding to a given sectors in the 1d representation of the hitmap
    std::map<const unsigned int, unsigned int> m_sectorBins1D;
    AIDA::IHistogram1D* m_1d_nClustersVsSector     = nullptr; ///< Number of clusters in each sector
    AIDA::IHistogram1D* m_1d_nClustersVsBeetlePort = nullptr; ///< Number of clusters in each beetle port

  };

  // Declaration of the backward compatible STCLusterMonitor class (not templated for the original ST case)
  using STClusterMonitor = STClusterMonitorT<>;

} // End of ST namespace
#endif // STCLUSTERMONITOR_H
