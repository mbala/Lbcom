#ifndef STSelectClustersByChannel_H
#define STSelectClustersByChannel_H 1

// ST tool base class
#include "Kernel/STToolBase.h"

// LHCbKernel
#include "Kernel/ISTClusterSelector.h"

/** @class STSelectClustersByChannel STSelectClustersByChannel.h
 *
 *  Tool for selecting clusters using a list of service boxes
 *
 *  @author M.Needham
 *  @date   3/2/2009
 */

struct ISTChannelIDSelector;

namespace LHCb{
  class STChannelID;
}

class STSelectClustersByChannel: public ST::ToolBase, 
                         virtual public ISTClusterSelector {

 public: 
   
  /// constructor
  STSelectClustersByChannel( const std::string& type,
                         const std::string& name,
                         const IInterface* parent );

  /// initialize
  virtual StatusCode initialize() override;

  /**  @param  cluster pointer to ST cluster object to be selected 
  *  @return true if cluster is selected
  */
  virtual bool select     ( const LHCb::STCluster* cluster ) const override;
  
  /** "select"/"preselect" method (functor interface)
   *  @param  cluster pointer to ST cluster object to be selected 
   *  @return true if cluster is selected
   */
  virtual bool operator() ( const LHCb::STCluster* cluster ) const override;

 private:

  ///   default  constructor  is  private 
  STSelectClustersByChannel();
  ///   copy     constructor  is  private 
  STSelectClustersByChannel (const STSelectClustersByChannel& );
  ///   assignement operator  is  private 
  STSelectClustersByChannel& operator= (const STSelectClustersByChannel& );  

  ISTChannelIDSelector* m_selector;
  std::string m_selectorType;
  std::string m_selectorName;
};

#endif // STSelectClustersByChannel_H
