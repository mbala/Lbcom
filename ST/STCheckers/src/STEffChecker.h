#ifndef STEffChecker_H
#define STEffChecker_H 1

// GaudiAlg
#include "Kernel/STCommonBase.h"
#include "Kernel/ISTReadoutTool.h"

// linkers
#include "Linker/LinkerTool.h"

namespace LHCb {
  class MCHit;
  class MCParticle;
  class STCluster;
  class STChannelID;
}

class DeSTLayer;
struct IMCParticleSelector;

namespace AIDA {
  class IHistogram1D;
  class IHistogram2D;
}

/** @class STEffCheckerT STEffChecker.h
 *
 *  Class for checking ST efficiencies. It produces the following plots:
 *  - x and y distributions of all MCHits in a IT or TT layer.
 *  - x vs y distribution of all MCHits in a IT or TT layer.
 *  - x and y distributions of MCHits which make an STCluster.
 *  - x vs y distribution of MCHits which make an STCluster.
 *  By dividing these histograms one gets the efficiency per layer.
 *
 *  In the finalize method, a summary of the efficiency per layer is printed. 
 * 
 *  @author M.Needham
 *  @author J. van Tilburg
 *  @date   21/12/2006
 */

template <class IReadoutTool = ISTReadoutTool>
class STEffCheckerT : public ST::CommonBase<GaudiHistoAlg, IReadoutTool> {

public:
 
  /// constructor
  STEffCheckerT(const std::string& name, ISvcLocator *svcloc );

  /// initialize
  StatusCode initialize() override;

  /// execute
  StatusCode execute() override;

  /// finalize
  StatusCode finalize() override;

private:

  typedef LinkerTool<LHCb::STCluster, LHCb::MCHit> AsctTool;

  virtual void initHistograms();
  
  virtual void layerEff(const LHCb::MCParticle* aParticle);

  bool isInside(const DeSTLayer* aLayer, const LHCb::MCHit* aHit) const;

  int findHistoId(unsigned int aLayerId);
  int uniqueHistoID(const LHCb::STChannelID aChannel) const;
 
  void unBookHistos();
  void eraseHistos(); 

  AsctTool::InverseType* m_table;

  std::string m_asctLocation;
  std::string m_clusterLocation;

  // pointer to p to hit associaton
  typedef LinkerTool< LHCb::MCHit, LHCb::MCParticle > HitTable;
  const HitTable::InverseType* m_hitTable;

  Gaudi::Property<std::string> m_selectorName{this, "SelectorName", "MCParticleSelector", "selector"};
  Gaudi::Property<bool> m_includeGuardRings{this, "IncludeGuardRings", false};
  Gaudi::Property<bool> m_pEff{this, "PrintEfficiency", true};
  IMCParticleSelector* m_selector;

  std::string m_hitTableLocation;
 
  // mapping
  std::map<unsigned int,int> m_mapping;

  std::vector<AIDA::IHistogram1D*> m_xLayerHistos;
  std::vector<AIDA::IHistogram1D*> m_yLayerHistos;
  std::vector<AIDA::IHistogram2D*> m_xyLayerHistos;
  std::vector<AIDA::IHistogram1D*> m_effXLayerHistos;
  std::vector<AIDA::IHistogram1D*> m_effYLayerHistos;
  std::vector<AIDA::IHistogram2D*> m_effXYLayerHistos;

};

// Declaration of the backward compatible STEffChecker class (not templated for the original ST case)
using STEffChecker = STEffCheckerT<>;

#endif // STEffChecker_H
