// Histogramming
#include "AIDA/IHistogram1D.h"
#include "AIDA/IHistogram2D.h"

// xml geometry
#include "STDet/DeSTDetector.h"
#include "STDet/DeSTLayer.h"

// Event
#include "Event/STCluster.h"
#include "Event/MCHit.h"
#include "Event/MCParticle.h"

// Kernel
#include "MCInterfaces/IMCParticleSelector.h"
#include "Kernel/STDetSwitch.h"
#include "Kernel/IUTReadoutTool.h"
#include "Kernel/STLexicalCaster.h"

// local
#include "HistFun.h"
#include "STEffChecker.h"

using namespace LHCb;

DECLARE_COMPONENT_WITH_ID( STEffCheckerT<ISTReadoutTool>, "STEffChecker" )
DECLARE_COMPONENT_WITH_ID( STEffCheckerT<IUTReadoutTool>, "UTEffChecker" )

//--------------------------------------------------------------------
//
//  STEffChecker: Plot the ST efficiency
//
//--------------------------------------------------------------------
template<class IReadoutTool>
STEffCheckerT<IReadoutTool>::STEffCheckerT( const std::string& name,
                            ISvcLocator* pSvcLocator ) :
  ST::CommonBase<GaudiHistoAlg, IReadoutTool>(name, pSvcLocator)
{
  this->declareSTConfigProperty("InputData" , m_clusterLocation  , STClusterLocation::TTClusters);
  this->setProperty( "HistoPrint", false );
  this->setForcedInit();
  this->declareSTConfigProperty("hitTableLocation", m_hitTableLocation , LHCb::MCParticleLocation::Default + "2MCTTHits");
  this->declareSTConfigProperty( "asctLocation", m_asctLocation , m_clusterLocation + "2MCHits");
}

template<class IReadoutTool>
StatusCode STEffCheckerT<IReadoutTool>::initialize()
{
  if( this->histoTopDir().empty() ) this->setHistoTopDir(this->detType()+"/");

  // Initialize GaudiHistoAlg
  StatusCode sc = ST::CommonBase<GaudiHistoAlg, IReadoutTool>::initialize();
  if (sc.isFailure()) return this->Error("Failed to initialize", sc);


  // MCParticle selector
  m_selector = this->template tool<IMCParticleSelector>(m_selectorName,m_selectorName,this);

  // init histos
  initHistograms();

  return sc;
}

template<class IReadoutTool>
void STEffCheckerT<IReadoutTool>::initHistograms()
{
  std::string tDirPath = this->histoPath()+"/";
  int numInVector = 0;
  int histID;

  // Intialize histograms
  for (const auto& layer : this->tracker()->layers()) {

    // uniquely id using station and layer
    // add to map
    STChannelID aChan = layer->elementID();
    unsigned int uniqueID = uniqueHistoID(aChan);
    m_mapping[uniqueID] = numInVector;

    // Create layer name
    std::string layerName = ", layer " + ST::toString((int)uniqueID);

    // Book x distribution histogram MCHits
    histID = (int)uniqueID+5000;
    IHistogram1D* xLayerHisto =
      this->book1D( histID, "x distribution hits"+layerName,-1000.,1000.,400);
    m_xLayerHistos.push_back(xLayerHisto);

    // Book y distribution histogram MCHits
    histID = (int)uniqueID+6000;
    IHistogram1D* yLayerHisto =
      this->book1D( histID, "y distribution hits"+layerName,-1000.,1000.,400);
    m_yLayerHistos.push_back(yLayerHisto);

    // Book x distribution histogram STClusters
    histID = (int)uniqueID+7000;
    IHistogram1D* effXLayerHisto =
      this->book1D( histID, "x distribution ass. clusters"+layerName,
              -1000.,1000.,400);
    m_effXLayerHistos.push_back(effXLayerHisto);

    // Book y distribution histogram STClusters
    histID = (int)uniqueID+8000;
    IHistogram1D* effYLayerHisto =
      this->book1D( histID, "y distribution ass. clusters"+layerName,
              -1000.,1000.,400);
    m_effYLayerHistos.push_back(effYLayerHisto);

    // Book x vs y distribution histogram MCHits
    histID = (int)uniqueID+9000;
    IHistogram2D* xyLayerHisto =
      this->book2D( histID, "x vs y distribution hits"+layerName,
              -1000.,1000., 200, -1000., 1000.,200);
    m_xyLayerHistos.push_back(xyLayerHisto);

    // Book x vs y distribution histogram STClusters
    histID = (int)uniqueID+10000;
    IHistogram2D* effXYLayerHisto =
      this->book2D( histID, "x vs y distribution ass. clusters"+layerName,
              -1000., 1000., 200, -1000., 1000., 200);
    m_effXYLayerHistos.push_back(effXYLayerHisto);

    ++numInVector;
  } // iLayer
}

template<class IReadoutTool>
StatusCode STEffCheckerT<IReadoutTool>::execute()
{
  // Get the MCParticles
  const MCParticles* particles = this->template get<MCParticles>(MCParticleLocation::Default);

  // Get the STCluster to MCHit associator
  AsctTool associator(this->evtSvc(), m_asctLocation);
  m_table = associator.inverse();
  if (!m_table) return this->Error("Failed to find table", StatusCode::FAILURE);

  // Get the MCParticle to MCHit associator
  HitTable hitAsct( this->evtSvc(), m_hitTableLocation );
  m_hitTable = hitAsct.inverse();
  if (!m_hitTable) return this->Error("Failed to find hit table at "
                                + m_hitTableLocation, StatusCode::FAILURE);

  for (const auto& particle : *particles) {
    if ( m_selector->accept(particle) ) layerEff(particle);
  }

  return StatusCode::SUCCESS;
}

template<class IReadoutTool>
StatusCode STEffCheckerT<IReadoutTool>::finalize()
{
  // init the message service
  if (m_pEff == true){
    this->info() << " -------Efficiency %--------- " << endmsg;
  }
  else {
    this->info() << " -------InEfficiency %--------- " << endmsg;
  }

  // print out efficiencys
  for (const auto& layer : this->tracker()->layers()) {

    STChannelID aChan = layer->elementID();
    int iHistoId = findHistoId(uniqueHistoID(aChan));

    // eff for this layer
    int nAcc =  m_xLayerHistos[iHistoId]->allEntries();
    int nFound = m_effXLayerHistos[iHistoId]->allEntries();
    double eff = 9999.;
    double err = 9999.;
    if (nAcc > 0){
      eff = 100.0*(double)nFound/(double)nAcc;
      err = sqrt((eff*(100.0-eff))/(double)nAcc);
      if (m_pEff == false) eff = 1-eff;
    }

    this->info() << this->uniqueLayer(aChan)
                 << " eff " << eff <<" +/- " << err << endmsg;

  } // layer

  this->info() << " -----------------------" << endmsg;


  // hack to prevent crash
  StatusCode sc = ST::CommonBase<GaudiHistoAlg, IReadoutTool>::finalize();
  if (sc.isFailure()){
    return this->Warning("Failed to finalize base class", sc);
  }

  // unbook if not full detail mode: histograms are not saved
  if (!this->fullDetail()){
    unBookHistos();
    eraseHistos();
  }

  return StatusCode::SUCCESS;
}

template<class IReadoutTool>
void STEffCheckerT<IReadoutTool>::layerEff(const MCParticle* aParticle)
{
  // find all MC hits for this particle
  auto hits = m_hitTable->relations(aParticle ) ;
  if (hits.empty()) return;

  for (const auto& layer : this->tracker()->layers()) {

    // look for MCHit in this layer.....

    std::vector<const MCHit*> layerHits;
    for ( const auto& link : hits ) {
      auto aHit = link.to();
      STChannelID hitChan = STChannelID(aHit->sensDetID());
      if ( uniqueHistoID( hitChan ) == uniqueHistoID(layer->elementID()) && isInside(layer,aHit)) {
          layerHits.push_back(aHit);
      }
    };

    if (!layerHits.empty()){
      bool found = std::any_of( layerHits.begin(), layerHits.end(),
                                [&](const MCHit* h) {
                return !m_table->relations(h).empty();
      });

      int iHistoId = findHistoId(uniqueHistoID(layer->elementID()));
      const MCHit* aHit = layerHits.front();
      const Gaudi::XYZPoint midPoint = aHit->midPoint();

      // histo vs x
      m_xLayerHistos[iHistoId]->fill( midPoint.x() );
      // histo vs y
      m_yLayerHistos[iHistoId]->fill( midPoint.y() );
      //  xy
      m_xyLayerHistos[iHistoId]->fill( midPoint.x(), midPoint.y());

      if (found){
        if (m_pEff){
          m_effXYLayerHistos[iHistoId]->fill( midPoint.x(), midPoint.y() );
          m_effXLayerHistos[iHistoId]->fill( midPoint.x() );
          m_effYLayerHistos[iHistoId]->fill( midPoint.y() );
        }
      }
      else {
        if (!m_pEff){
          m_effXYLayerHistos[iHistoId]->fill( midPoint.x(), midPoint.y() );
          m_effXLayerHistos[iHistoId]->fill( midPoint.x() );
          m_effYLayerHistos[iHistoId]->fill( midPoint.y() );
        }
      }
    } //if
  } // layer
}

template<class IReadoutTool>
int STEffCheckerT<IReadoutTool>::findHistoId(unsigned int aLayerId)
{
  return m_mapping[aLayerId];
}

template<class IReadoutTool>
int STEffCheckerT<IReadoutTool>::uniqueHistoID(const STChannelID aChan) const
{
  return ( this->detType() == "TT" || this->detType() == "UT") ?
    aChan.station()*100 + aChan.layer()  :
    aChan.station()*100 + aChan.detRegion()*10 + aChan.layer();
}

template<class IReadoutTool>
bool STEffCheckerT<IReadoutTool>::isInside(const DeSTLayer* aLayer, const MCHit* aHit) const
{
  // check if expect hit to make cluster
  if (!aLayer->isInside(aHit->midPoint())) return false;
  if (m_includeGuardRings) return true;
  const DeSTSector* aSector = this->tracker()->findSector(aHit->midPoint());
  return aSector && aSector->globalInActive(aHit->midPoint());
}

template<class IReadoutTool>
void STEffCheckerT<IReadoutTool>::unBookHistos(){

  // give ownership back to vector - histos no longer in store
  HistFun::unBookVector(m_xLayerHistos,     this->histoSvc());
  HistFun::unBookVector(m_yLayerHistos,     this->histoSvc());
  HistFun::unBookVector(m_xyLayerHistos,    this->histoSvc());
  HistFun::unBookVector(m_effXLayerHistos,  this->histoSvc());
  HistFun::unBookVector(m_effYLayerHistos,  this->histoSvc());
  HistFun::unBookVector(m_effXYLayerHistos, this->histoSvc());
}

template<class IReadoutTool>
void STEffCheckerT<IReadoutTool>::eraseHistos(){

  // clear everything
  HistFun::eraseVector(m_xLayerHistos);
  HistFun::eraseVector(m_yLayerHistos);
  HistFun::eraseVector(m_xyLayerHistos);
  HistFun::eraseVector(m_effXLayerHistos);
  HistFun::eraseVector(m_effYLayerHistos);
  HistFun::eraseVector(m_effXYLayerHistos);
}

