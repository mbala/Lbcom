#ifndef HC_DIGIT_MONITOR_H
#define HC_DIGIT_MONITOR_H 1

//#define ONLINEMONITORING

// AIDA
#include "AIDA/IHistogram1D.h"
#include "AIDA/IHistogram2D.h"
#include "AIDA/IProfile1D.h"

// Local
#include "HCMonitorBase.h"

/** @class HCDigitMonitor HCDigitMonitor.h
 *
 *  Monitoring algorithm for Herschel digits.
 *
 */

class HCDigitMonitor final : public HCMonitorBase {
 public:
  /// Standard constructor
  HCDigitMonitor(const std::string& name, ISvcLocator* pSvcLocator);

  virtual StatusCode initialize() override;  ///< Algorithm initialization
  virtual StatusCode execute()    override;  ///< Algorithm execution
  virtual StatusCode finalize()   override;  ///< Algorithm finalization

 private:
  /// TES location of digits
  std::string m_digitLocation;

#ifdef ONLINEMONITORING
  /// Data buffer for the filling scheme
  std::string m_schema[2];
  /// DIM service ID for the filling scheme
  int m_schemaSvc[2] = {0, 0};
#endif

  /// Flag whether to book and fill the ADC vs. BX 2D histograms.
  bool m_adcVsBx; 
  /// Flag whether to fill the ADC distribution histograms for all channels.
  bool m_allChannels;

  /// ADC distributions of all channels
  std::vector<std::vector<AIDA::IHistogram1D*> > m_hAdcChannel;

  /// ADC distributions of all quadrants
  std::vector<std::vector<AIDA::IHistogram1D*> > m_hAdcQuadrant;
  /// ADC distributions of all quadrants for empty-empty crossings
  std::vector<std::vector<AIDA::IHistogram1D*> > m_hAdcQuadrantNoBeam;
  /// ADC distributions of all quadrants for beam-beam crossings
  std::vector<std::vector<AIDA::IHistogram1D*> > m_hAdcQuadrantBeam;
  /// ADC distributions of all quadrants for spill-over crossings
  std::vector<AIDA::IHistogram1D*> m_hAdcQuadrantSpillOver;
  /// ADC distributions of all quadrants for pre-spill crossings
  std::vector<AIDA::IHistogram1D*> m_hAdcQuadrantPreSpill;
  /// ADC distributions of all quadrants for "empty" crossings
  std::vector<AIDA::IHistogram1D*> m_hAdcQuadrantEmpty;

  /// ADC distributions as function of BX ID.
  std::vector<AIDA::IHistogram2D*> m_hAdcVsBx;

  /// ADC sum distributions
  std::vector<std::vector<AIDA::IHistogram1D*> > m_hAdcSum;
  /// ADC sum distributions for empty-empty crossings
  std::vector<std::vector<AIDA::IHistogram1D*> > m_hAdcSumNoBeam;
  /// ADC sum distributions for beam-beam crossings
  std::vector<std::vector<AIDA::IHistogram1D*> > m_hAdcSumBeam;

  /// Mean ADC vs. channel number
  std::vector<std::vector<AIDA::IProfile1D*> > m_hAdcVsChannel;

  /// Mean ADC vs. quadrant
  std::vector<std::vector<AIDA::IProfile1D*> > m_hAdcVsQuadrant;
  /// Mean ADC vs. quadrant for empty-empty crossings
  std::vector<std::vector<AIDA::IProfile1D*> > m_hAdcVsQuadrantNoBeam;
  /// Mean ADC vs. quadrant for beam-beam crossings
  std::vector<std::vector<AIDA::IProfile1D*> > m_hAdcVsQuadrantBeam;

  /// Correlation plots
  std::vector<AIDA::IHistogram2D*> m_hCorrelation;

  /// Histogram to spot missing data.
  AIDA::IHistogram1D* m_hMissingQuads = nullptr;
  /// Bunch-crossing types
  AIDA::IHistogram1D* m_hBxTypes = nullptr;
  /// Trigger types broadcasted by ODIN
  AIDA::IHistogram1D* m_hTriggerTypes = nullptr;
  /// Routing bits that fired
  AIDA::IHistogram1D* m_hRoutingBits = nullptr;
  /// Visualisation of the filling scheme (for debugging).
  AIDA::IHistogram2D* m_hFillingScheme = nullptr;

  Condition* m_condFilling = nullptr;
  std::bitset<3564> m_isSpillOver;
  std::bitset<3564> m_isPreSpill;
  std::bitset<3564> m_isEmpty;

  StatusCode cacheFillingScheme();
  bool updateBxIds(const std::string& b1, const std::string& b2);
};

#endif
