//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Fri May 26 20:14:33 2017 by ROOT version 6.08/06
// from TTree HCDigitTuple/Herschel digits
// found on file: /home/herschel/AnalysedRuns/191875/NTuple_191875Tuple.root
//////////////////////////////////////////////////////////

#ifndef HRCTuple_h
#define HRCTuple_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.

class HRCTuple {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   UInt_t          run;
   ULong64_t       eventID;
   ULong64_t       orbitID;
   UInt_t          bxID;
   ULong64_t       odinTime;
   Int_t           GpsYear;
   Int_t           GpsMonth;
   Int_t           GpsDay;
   Int_t           GpsHour;
   Int_t           GpsMinute;
   Double_t        GpsSecond;
   Int_t           slot;
   Int_t           step;
   Float_t         adc_B[64];
   Float_t         adc_F[64];
   Int_t           fill;
   Int_t           B00;
   Int_t           B00_reference;
   Int_t           B01;
   Int_t           B01_reference;
   Int_t           B02;
   Int_t           B02_reference;
   Int_t           B03;
   Int_t           B03_reference;
   Int_t           B10;
   Int_t           B10_reference;
   Int_t           B11;
   Int_t           B11_reference;
   Int_t           B12;
   Int_t           B12_reference;
   Int_t           B13;
   Int_t           B13_reference;
   Int_t           B20;
   Int_t           B20_reference;
   Int_t           B21;
   Int_t           B21_reference;
   Int_t           B22;
   Int_t           B22_reference;
   Int_t           B23;
   Int_t           B23_reference;
   Int_t           F10;
   Int_t           F10_reference;
   Int_t           F11;
   Int_t           F11_reference;
   Int_t           F12;
   Int_t           F12_reference;
   Int_t           F13;
   Int_t           F13_reference;
   Int_t           F20;
   Int_t           F20_reference;
   Int_t           F21;
   Int_t           F21_reference;
   Int_t           F22;
   Int_t           F22_reference;
   Int_t           F23;
   Int_t           F23_reference;

   // List of branches
   TBranch        *b_run;   //!
   TBranch        *b_eventID;   //!
   TBranch        *b_orbitID;   //!
   TBranch        *b_bxID;   //!
   TBranch        *b_odinTime;   //!
   TBranch        *b_GpsYear;   //!
   TBranch        *b_GpsMonth;   //!
   TBranch        *b_GpsDay;   //!
   TBranch        *b_GpsHour;   //!
   TBranch        *b_GpsMinute;   //!
   TBranch        *b_GpsSecond;   //!
   TBranch        *b_slot;   //!
   TBranch        *b_step;   //!
   TBranch        *b_adc_B;   //!
   TBranch        *b_adc_F;   //!
   TBranch        *b_fill;   //!
   TBranch        *b_B00;   //!
   TBranch        *b_B00_reference;   //!
   TBranch        *b_B01;   //!
   TBranch        *b_B01_reference;   //!
   TBranch        *b_B02;   //!
   TBranch        *b_B02_reference;   //!
   TBranch        *b_B03;   //!
   TBranch        *b_B03_reference;   //!
   TBranch        *b_B10;   //!
   TBranch        *b_B10_reference;   //!
   TBranch        *b_B11;   //!
   TBranch        *b_B11_reference;   //!
   TBranch        *b_B12;   //!
   TBranch        *b_B12_reference;   //!
   TBranch        *b_B13;   //!
   TBranch        *b_B13_reference;   //!
   TBranch        *b_B20;   //!
   TBranch        *b_B20_reference;   //!
   TBranch        *b_B21;   //!
   TBranch        *b_B21_reference;   //!
   TBranch        *b_B22;   //!
   TBranch        *b_B22_reference;   //!
   TBranch        *b_B23;   //!
   TBranch        *b_B23_reference;   //!
   TBranch        *b_F10;   //!
   TBranch        *b_F10_reference;   //!
   TBranch        *b_F11;   //!
   TBranch        *b_F11_reference;   //!
   TBranch        *b_F12;   //!
   TBranch        *b_F12_reference;   //!
   TBranch        *b_F13;   //!
   TBranch        *b_F13_reference;   //!
   TBranch        *b_F20;   //!
   TBranch        *b_F20_reference;   //!
   TBranch        *b_F21;   //!
   TBranch        *b_F21_reference;   //!
   TBranch        *b_F22;   //!
   TBranch        *b_F22_reference;   //!
   TBranch        *b_F23;   //!
   TBranch        *b_F23_reference;   //!

   HRCTuple(TString, TString, TTree *tree=0);
   virtual ~HRCTuple();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef HRCTuple_cxx
HRCTuple::HRCTuple(TString fileName , TString treeName , TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0 && fileName != "") {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject(fileName);
      if (!f || !f->IsOpen()) {
         f = new TFile(fileName);
      }
      TDirectory * dir = (TDirectory*)f->Get(fileName+":/"+treeName);
      dir->GetObject(treeName,tree);

   Init(tree);
   }
}

HRCTuple::~HRCTuple()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t HRCTuple::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t HRCTuple::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void HRCTuple::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("run", &run, &b_run);
   fChain->SetBranchAddress("eventID", &eventID, &b_eventID);
   fChain->SetBranchAddress("orbitID", &orbitID, &b_orbitID);
   fChain->SetBranchAddress("bxID", &bxID, &b_bxID);
   fChain->SetBranchAddress("odinTime", &odinTime, &b_odinTime);
   fChain->SetBranchAddress("GpsYear", &GpsYear, &b_GpsYear);
   fChain->SetBranchAddress("GpsMonth", &GpsMonth, &b_GpsMonth);
   fChain->SetBranchAddress("GpsDay", &GpsDay, &b_GpsDay);
   fChain->SetBranchAddress("GpsHour", &GpsHour, &b_GpsHour);
   fChain->SetBranchAddress("GpsMinute", &GpsMinute, &b_GpsMinute);
   fChain->SetBranchAddress("GpsSecond", &GpsSecond, &b_GpsSecond);
   fChain->SetBranchAddress("slot", &slot, &b_slot);
   fChain->SetBranchAddress("step", &step, &b_step);
   fChain->SetBranchAddress("adc_B", adc_B, &b_adc_B);
   fChain->SetBranchAddress("adc_F", adc_F, &b_adc_F);
   fChain->SetBranchAddress("fill", &fill, &b_fill);
   fChain->SetBranchAddress("B00", &B00, &b_B00);
   fChain->SetBranchAddress("B00_reference", &B00_reference, &b_B00_reference);
   fChain->SetBranchAddress("B01", &B01, &b_B01);
   fChain->SetBranchAddress("B01_reference", &B01_reference, &b_B01_reference);
   fChain->SetBranchAddress("B02", &B02, &b_B02);
   fChain->SetBranchAddress("B02_reference", &B02_reference, &b_B02_reference);
   fChain->SetBranchAddress("B03", &B03, &b_B03);
   fChain->SetBranchAddress("B03_reference", &B03_reference, &b_B03_reference);
   fChain->SetBranchAddress("B10", &B10, &b_B10);
   fChain->SetBranchAddress("B10_reference", &B10_reference, &b_B10_reference);
   fChain->SetBranchAddress("B11", &B11, &b_B11);
   fChain->SetBranchAddress("B11_reference", &B11_reference, &b_B11_reference);
   fChain->SetBranchAddress("B12", &B12, &b_B12);
   fChain->SetBranchAddress("B12_reference", &B12_reference, &b_B12_reference);
   fChain->SetBranchAddress("B13", &B13, &b_B13);
   fChain->SetBranchAddress("B13_reference", &B13_reference, &b_B13_reference);
   fChain->SetBranchAddress("B20", &B20, &b_B20);
   fChain->SetBranchAddress("B20_reference", &B20_reference, &b_B20_reference);
   fChain->SetBranchAddress("B21", &B21, &b_B21);
   fChain->SetBranchAddress("B21_reference", &B21_reference, &b_B21_reference);
   fChain->SetBranchAddress("B22", &B22, &b_B22);
   fChain->SetBranchAddress("B22_reference", &B22_reference, &b_B22_reference);
   fChain->SetBranchAddress("B23", &B23, &b_B23);
   fChain->SetBranchAddress("B23_reference", &B23_reference, &b_B23_reference);
   fChain->SetBranchAddress("F10", &F10, &b_F10);
   fChain->SetBranchAddress("F10_reference", &F10_reference, &b_F10_reference);
   fChain->SetBranchAddress("F11", &F11, &b_F11);
   fChain->SetBranchAddress("F11_reference", &F11_reference, &b_F11_reference);
   fChain->SetBranchAddress("F12", &F12, &b_F12);
   fChain->SetBranchAddress("F12_reference", &F12_reference, &b_F12_reference);
   fChain->SetBranchAddress("F13", &F13, &b_F13);
   fChain->SetBranchAddress("F13_reference", &F13_reference, &b_F13_reference);
   fChain->SetBranchAddress("F20", &F20, &b_F20);
   fChain->SetBranchAddress("F20_reference", &F20_reference, &b_F20_reference);
   fChain->SetBranchAddress("F21", &F21, &b_F21);
   fChain->SetBranchAddress("F21_reference", &F21_reference, &b_F21_reference);
   fChain->SetBranchAddress("F22", &F22, &b_F22);
   fChain->SetBranchAddress("F22_reference", &F22_reference, &b_F22_reference);
   fChain->SetBranchAddress("F23", &F23, &b_F23);
   fChain->SetBranchAddress("F23_reference", &F23_reference, &b_F23_reference);
   Notify();
}

Bool_t HRCTuple::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void HRCTuple::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t HRCTuple::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef HRCTuple_cxx
